﻿using Api.Clouds.ModuleBase;
using Api.Clouds.ModuleBase.Utils;
using CG.Web.MegaApiClient;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace Api.Clouds.Modules.Mega
{
    [Export(typeof(IModule)), PartCreationPolicy(CreationPolicy.NonShared)]
    [ExportModule(ModuleName = "Mega", AuthenticationType = AuthenticationType.Simple)]
    public class MegaModule : IModule, INotifyPropertyChanged
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private MegaApiClient _client;
        private INode _uploadFolder;

        #region Properties

        private bool _isAuthenticated;
        public string IconName => AppDomain.CurrentDomain.BaseDirectory + @"Resources\Images\mega-icon.png";

        public bool IsAuthenticated
        {
            get
            {
                return _isAuthenticated;
            }
            set
            {
                if (_isAuthenticated == value) return;
                _isAuthenticated = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsAuthenticated"));
            }
        }

        public string ModuleName => "Mega";

        #endregion Properties

        #region IModule

        [ImportingConstructor]
        public MegaModule()
        {
        }

        public void Dispose()
        {
        }

        #endregion IModule

        #region Sync

        public LoadProcess Download(Stream stream, string cloudFileName)
        {
            if (!IsAuthenticated)
            {
                _logger.Error("Module: Mega; Method: Download; Error: not authenicated.");
                return null;
            }

            try
            {
                var nodes = new List<INode>(_client.GetNodes());
                var node = nodes.Find(n => n.Name == cloudFileName);

                var downloadProcess = new LoadProcess();

                var stopwatch = Stopwatch.StartNew();
                _client.Download(node).CopyTo(stream);
                stopwatch.Stop();

                downloadProcess.ElapsedTime = stopwatch.Elapsed;

                downloadProcess.FileInfo = new FileInfo((stream as FileStream)?.Name);

                return downloadProcess;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Module: Mega; Method: Download; Error: download failed.");
                return null;
            }
        }

        public LoadProcess Download(string localFileName, string cloudFileName)
        {
            if (!IsAuthenticated)
            {
                _logger.Error("Module: Mega; Method: Download; Error: not authenicated.");
                return null;
            }

            try
            {
                var nodes = new List<INode>(_client.GetNodes());
                var node = nodes.Find(n => n.Name == cloudFileName);

                var downloadProcess = new LoadProcess();

                using (var fileStream = new FileStream(localFileName, FileMode.Create, FileAccess.Write))
                {
                    var stopwatch = Stopwatch.StartNew();
                    _client.Download(node).CopyTo(fileStream);
                    stopwatch.Stop();

                    downloadProcess.ElapsedTime = stopwatch.Elapsed;
                }

                downloadProcess.FileInfo = new FileInfo(localFileName);

                return downloadProcess;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Module: Mega; Method: Download; Error: download failed.");
                return null;
            }
        }

        public List<string> GetFiles()
        {
            if (!IsAuthenticated)
            {
                _logger.Warn("Mega module: GetFiles failed. Not authenticated.");
                return null;
            }

            List<INode> nodes = new List<INode>();

            try
            {
                nodes.AddRange(_client.GetNodes());
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Mega module: GetFiles method. Error getting files.");
                return null;
            }

            List<string> result = new List<string>();
            nodes.ForEach(x => result.Add(x.Name));

            return result;
        }

        public LoadProcess Upload(Stream stream, string cloudFileName)
        {
            if (!IsAuthenticated)
            {
                _logger.Error("Module: Mega; Method: Upload; Error: not authenticated.");
                return null;
            }

            try
            {
                var nodes = new List<INode>(_client.GetNodes());
                var node = nodes.Find(n => n.Name == cloudFileName);

                if (node != null)
                {
                    _logger.Warn("Module: Mega; Method: Upload; Warn: file already uploaded. Deleting...");
                    _client.Delete(node);
                }

                var uploadProcess = new LoadProcess()
                {
                    FileInfo = new FileInfo((stream as FileStream)?.Name),
                };

                var stopwatch = Stopwatch.StartNew();
                var file = _client.Upload(stream, cloudFileName, _uploadFolder);
                stopwatch.Stop();

                if (file == null)
                {
                    _logger.Error("Module: Mega; Method: Upload; Error: upload failed.");
                    return null;
                }

                uploadProcess.ElapsedTime = stopwatch.Elapsed;

                return uploadProcess;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Module: Mega; Method: Upload; Error: upload failed.");
                return null;
            }
        }

        public LoadProcess Upload(string localFileName, string cloudFileName)
        {
            if (!IsAuthenticated)
            {
                _logger.Error("Module: Mega; Method: Upload; Error: not authenticated.");
                return null;
            }
            if (!File.Exists(localFileName))
            {
                _logger.Error("Module: Mega; Method: Upload; Error: file not exists.");
                return null;
            }

            try
            {
                var nodes = new List<INode>(_client.GetNodes());
                var node = nodes.Find(n => n.Name == cloudFileName);

                if (node != null)
                {
                    _logger.Warn("Module: Mega; Method: Upload; Warn: file already uploaded. Deleting...");
                    _client.Delete(node);
                }

                var uploadProcess = new LoadProcess()
                {
                    FileInfo = new FileInfo(localFileName),
                };

                using (var fileStream = new FileStream(localFileName, FileMode.Open, FileAccess.Read))
                {
                    var stopwatch = Stopwatch.StartNew();
                    var file = _client.Upload(fileStream, cloudFileName, _uploadFolder);
                    stopwatch.Stop();

                    if (file == null)
                    {
                        _logger.Error("Module: Mega; Method: Upload; Error: upload failed.");
                        return null;
                    }

                    uploadProcess.ElapsedTime = stopwatch.Elapsed;

                    return uploadProcess;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Module: Mega; Method: Upload; Error: upload failed.");
                return null;
            }
        }

        #endregion Sync

        #region Async

        public async Task<LoadProcess> DownloadFileAsync(Stream stream, string cloudFileName)
        {
            return await Task.Run(() => Download(stream, cloudFileName));
        }

        public async Task<LoadProcess> DownloadFileAsync(string localFileName, string cloudFileName)
        {
            return await Task.Run(() => Download(localFileName, cloudFileName));
        }

        public async Task<LoadProcess> UploadFileAsync(Stream stream, string cloudFileName)
        {
            return await Task.Run(() => Upload(stream, cloudFileName));
        }

        public async Task<LoadProcess> UploadFileAsync(string localFileName, string cloudFileName)
        {
            return await Task.Run(() => Upload(localFileName, cloudFileName));
        }

        #endregion Async

        #region Authentication

        public async Task Authenticate(AccountInfo accountInfo)
        {
            await Task.Run(() =>
            {
                _logger.Info("Mega module: Auth started.");
                _client = new MegaApiClient();
                try
                {
                    var accountMega = accountInfo as AccountMega;
                    if (accountMega != null)
                        _client.Login(accountMega.Login, accountMega.Password);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Mega module: Auth failed!");
                    return;
                }
                _logger.Info("Mega module: Auth succseed");

                List<INode> nodes;
                try
                {
                    nodes = new List<INode>(_client.GetNodes());
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Mega module: Error getting nodes!");
                    return;
                }
                var root = nodes.Find(n => n.Type == NodeType.Root);
                if (root == null)
                {
                    _logger.Error("Mega module: Root folder not found!");
                    return;
                }
                _uploadFolder = nodes.Find(n => n.Name == "Upload");
                if (_uploadFolder == null)
                {
                    _logger.Warn("Mega module: Upload folder not found! Creating new...");
                    try
                    {
                        _client.CreateFolder("Upload", root);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex, "Mega module: Can't create new Upload folder!");
                        return;
                    }
                }
                IsAuthenticated = true;
                _logger.Info("Mega module: Authentication complete.");
            });
        }

        public async Task Authenticate()
        {
            var ls = new LoginScreen();
            ls.ShowDialog();

            var accInfo = new AccountMega()
            {
                Login = ls.Login.Text,
                Password = ls.Password.Password,
            };

            await Authenticate(accInfo);
        }

        #endregion Authentication

        public event PropertyChangedEventHandler PropertyChanged;
    }
}