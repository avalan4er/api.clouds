﻿using Api.Clouds.ModuleBase;
using Api.Clouds.Modules.Box;
using Api.Clouds.Modules.DropBox;
using Api.Clouds.Modules.GoogleDrive;
using Api.Clouds.Modules.Mega;
using Api.Clouds.Modules.OneDrive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Api.Clouds.Console
{
    internal class ModulesManager
    {
        public ModulesManager()
        {
            Box = new BoxModule();
            DropBox = new DropBoxModule();
            GoogleDrive = new GoogleDriveModule();
            Mega = new MegaModule();
            OneDrive = new OneDriveModule();
        }

        #region Properties

        public IModule Box { get; private set; }
        public IModule DropBox { get; private set; }
        public IModule GoogleDrive { get; private set; }
        public IModule Mega { get; private set; }
        public IModule OneDrive { get; private set; }

        #endregion Properties

        #region Commands

        public void AuthAll()
        {
            try
            {
                var app = new Application();
                app.Run();

                app.Dispatcher.Invoke(() =>
                {
                    foreach (var module in GetAllModules())
                    {
                        module.Authenticate();
                    }
                });
            }
            catch (Exception ex)
            {
            }
        }

        public List<IModule> GetAllModules()
        {
            return new List<IModule>()
            {
                Box,
                DropBox,
                GoogleDrive,
                Mega,
                OneDrive
            };
        }

        #endregion Commands
    }
}