﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Api.Clouds.FileUploader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer _timer;

        public MainWindow()
        {
            InitializeComponent();

            var uploader = new Uploader();

            _timer = new DispatcherTimer();
            _timer.Interval = TimeSpan.FromMinutes(10);
            _timer.Tick += async (s, e) =>
            {
                Console.Text += $"\nНачало загрузки: {DateTime.Now}";
                await uploader.Upload();
                Console.Text += uploader.CurrentReport;
                Console.Text += $"\nКонец загрузки: {DateTime.Now}";
                Console.Text += "\n ---------------------------- \n\n";
            };
        }

        private void StartUploading_Click(object sender, RoutedEventArgs e)
        {
            _timer.Start();
            Console.Text += "\nПроцесс загрузки запущен. Первый отчет появится через 10-15 минут.";
        }

        private void StopUploading_Click(object sender, RoutedEventArgs e)
        {
            _timer.Stop();
        }
    }
}
