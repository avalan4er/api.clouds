﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Api.Clouds.ModuleBase;
using Api.Clouds.ModuleBase.Utils;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v2;
using Google.Apis.Services;
using NLog;
using static System.IO.Path;

namespace Api.Clouds.Modules.GoogleDrive
{
    [Export(typeof(IModule)), PartCreationPolicy(CreationPolicy.NonShared)]
    [ExportModule(ModuleName = "GoogleDrive", AuthenticationType = AuthenticationType.OAuth)]
    public class GoogleDriveModule : IModule, INotifyPropertyChanged
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        DriveService _service;

        #region IModule
        public GoogleDriveModule()
        {
            _service = null;
        }
        public void Dispose()
        {
            _service.Dispose();
        }
        #endregion

        #region Properties
        private bool _isAuthenticated;
        public bool IsAuthenticated
        {
            get
            {
                return _isAuthenticated;
            }
            set
            {
                if (_isAuthenticated == value) return;
                _isAuthenticated = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("IsAuthenticated"));
                }
            }
        }

        public string ModuleName => "GoogleDrive";

        public string IconName => AppDomain.CurrentDomain.BaseDirectory + @"Resources\Images\googledrive-icon.png";
        #endregion

        #region Authentication
        public async Task Authenticate(AccountInfo accountInfo)
        {
            await Authenticate();
        }
        public async Task Authenticate()
        {
            _logger.Info("GoogleDrive module: Authenicate start.");

            UserCredential credential;
            try {
                credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    new ClientSecrets
                    {
                        ClientId = "697499478089-ro0d2bu98ij4jsbddden6icr5hehu9pj.apps.googleusercontent.com",
                        ClientSecret = "VLmvar2uPNwDRgYIl2iJpzd_",
                    },
                    new[] { DriveService.Scope.Drive },
                    "user",
                    CancellationToken.None);
            } catch (Exception ex)
            {
                _logger.Error(ex, "GoogleDrive module: Auth error.");
                return;
            }

            if (credential.Token == null)
            {
                _logger.Error("GoogleDrive module: Auth error. Token is null.");
                return;
            }

            _logger.Info("GoogleDrive module: Credential received.");

            _logger.Info("GoogleDrive module: Creating servce.");
            _service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "ApiCloudsModulesGoogleDrive",
            });
            _logger.Info("GoogleDrive module: Service created and started.");

            IsAuthenticated = true;
            _logger.Info("GoogleDrive module: Authentication complete.");
        }
        #endregion

        #region Async

        public async Task<LoadProcess> UploadFileAsync(Stream stream, string cloudFileName)
        {
            return await Task.Run(() => Upload(stream, cloudFileName));
        }

        public async Task<LoadProcess> UploadFileAsync(string localFileName, string cloudFileName)
        {
            return await Task.Run(() => Upload(localFileName, cloudFileName));
        }

        public async Task<LoadProcess> DownloadFileAsync(Stream stream, string cloudFileName)
        {
            return await Task.Run(() => Download(stream, cloudFileName));
        }

        public async Task<LoadProcess> DownloadFileAsync(string localFileName, string cloudFileName)
        {
            return await Task.Run(() => Download(localFileName, cloudFileName));
        }
        #endregion

        #region Sync

        public LoadProcess Upload(Stream stream, string cloudFileName)
        {
            if (!IsAuthenticated)
            {
                _logger.Error("Module: GoogleDrive; Method: Upload; Error: not authenticated.");
                return null;
            }

            var uploadProcess = new LoadProcess
            {
                FileInfo = new FileInfo((stream as FileStream)?.Name),
            };

            var files = _service.Files.List().Execute();
            var fileToDelete = files.Items.FirstOrDefault(x => x.Title.Contains(cloudFileName));
            if (fileToDelete != null)
            {
                _logger.Warn("Module: GoogleDrive; Method: Upload; File already exists. Deleting...");
                _service.Files.Delete(fileToDelete.Id).Execute();
                _logger.Warn("Module: GoogleDrive; Method: Upload; File deleted.");
            }

            var fileToUpload = new Google.Apis.Drive.v2.Data.File()
            {
                Title = cloudFileName,
            };

            var request = _service.Files.Insert(fileToUpload, stream, "application/octet-stream");

            _logger.Trace("Module: GoogleDrive; Method: Upload; File name: {0}; Start uploading...");
            var stopwatch = Stopwatch.StartNew();
            request.Upload();
            stopwatch.Stop();
            uploadProcess.ElapsedTime = stopwatch.Elapsed;

            var response = request.ResponseBody;
            if (response.Id != null)
            {
                _logger.Trace("Module: GoogleDrive; Method: Upload; File name: {0}; Done uploading file.");
                return uploadProcess;
            }

            _logger.Error("Module: GoogleDrive; Method: Upload; Error uploading file");
            return null;
        }

        public LoadProcess Upload(string localFileName, string cloudFileName)
        {
            if (!IsAuthenticated)
            {
                _logger.Error("Module: GoogleDrive; Method: Upload; Error: not authenticated.");
                return null;
            }

            if (!File.Exists(localFileName))
            {
                _logger.Error("Module: GoogleDrive; Method: Upload; Error: file not exisits.");
                return null;
            }

            var uploadProcess = new LoadProcess
            {
                FileInfo = new FileInfo(localFileName),
            };

            var files = _service.Files.List().Execute();
            var fileToDelete = files.Items.FirstOrDefault(x => x.Title.Contains(cloudFileName));
            if (fileToDelete != null)
            {
                _logger.Warn("Module: GoogleDrive; Method: Upload; File already exists. Deleting...");
                _service.Files.Delete(fileToDelete.Id).Execute();
                _logger.Warn("Module: GoogleDrive; Method: Upload; File deleted.");
            }

            var fileToUpload = new Google.Apis.Drive.v2.Data.File()
            {
                Title = cloudFileName,
            };

            using (var fileStream = new FileStream(localFileName, FileMode.Open, FileAccess.Read))
            {
                var request = _service.Files.Insert(fileToUpload, fileStream, "application/octet-stream");

                var stopwatch = Stopwatch.StartNew();
                request.Upload();
                stopwatch.Stop();
                uploadProcess.ElapsedTime = stopwatch.Elapsed;

                var response = request.ResponseBody;
                if (response.Id != null) return uploadProcess;

                _logger.Error("Module: GoogleDrive; Method: Upload; Error uploading file");
                return null;
            }
        }

        public LoadProcess Download(Stream stream, string cloudFileName)
        {
            if (_service == null)
            {
                throw new InvalidOperationException("_service");
            }
            if (!IsAuthenticated)
            {
                _logger.Error("Module: GoogleDrive; Method: Download; Error: not authenticated.");
                return null;
            }

            var files = _service.Files.List().Execute();
            // ReSharper disable once AssignNullToNotNullAttribute
            var fileToDownload = files.Items.SingleOrDefault(x => x.Title.Contains(GetFileName(cloudFileName)));
            if (fileToDownload == null)
            {
                _logger.Error("Module: GoogleDrive; Method: Download; Error: download failed, file not found.");
                return null;
            }

            var request = _service.Files.Get(fileToDownload.Id);

            var stopwatch = Stopwatch.StartNew();
            request.Download(stream);
            stopwatch.Stop();

            return new LoadProcess
            {
                ElapsedTime = stopwatch.Elapsed,
                FileInfo = new FileInfo((stream as FileStream)?.Name)
            };
        }

        public LoadProcess Download(string localFileName, string cloudFileName)
        {
            if (_service == null)
            {
                throw new InvalidOperationException("_service");
            }
            if (!IsAuthenticated)
            {
                _logger.Error("Module: GoogleDrive; Method: Download; Error: not authenticated.");
                return null;
            }

            var files = _service.Files.List().Execute();
            // ReSharper disable once AssignNullToNotNullAttribute
            var fileToDownload = files.Items.SingleOrDefault(x => x.Title.Contains(GetFileName(cloudFileName)));
            if (fileToDownload == null)
            {
                _logger.Error("Module: GoogleDrive; Method: Download; Error: download failed, file not found.");
                return null;
            }

            var request = _service.Files.Get(fileToDownload.Id);

            var stopwatch = Stopwatch.StartNew();
            using (var fileStream = new FileStream(localFileName, FileMode.Create, FileAccess.Write))
            {
                request.Download(fileStream);
            }
            stopwatch.Stop();

            return new LoadProcess
            {
                ElapsedTime = stopwatch.Elapsed,
                FileInfo = new FileInfo(localFileName)
            };
        }

        public List<string> GetFiles()
        {
            if (!IsAuthenticated)
            {
                _logger.Warn("GoogleDrive module: GetFiles failed. not authenticated.");
                return null;
            }

            List<Google.Apis.Drive.v2.Data.File> fileList;

            try
            {
                var request = _service.Files.List();
                fileList = request.Execute().Items.ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "GoogleDrive module: GetFiles method. Error getting files.");
                return null;
            }

            var result = new List<string>();
            fileList.ForEach(x => result.Add(x.Title));

            return result;
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
