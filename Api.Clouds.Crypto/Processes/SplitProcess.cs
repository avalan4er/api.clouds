﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Api.Clouds.Common.Numerics;
using Api.Clouds.Crypto.Algorithms;
using Api.Clouds.Crypto.Share;
using NLog;
using NLog.Targets;

namespace Api.Clouds.Crypto.Processes
{
    public class SplitProcess
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public void PerformSplit(string path, int N)
        {
            if (!File.Exists(path))
            {
                _logger.Error("Module: Crypto; Method: PerformSplit; Error: File not exists. File name: {0}", path);
            }

            var bufferLength = ((2 * N) / 8) - 1;

            //Create second level modules
            var secondLevelModules = new []
            {
                (N+1)/2,
                (N+1)/2,
                (N+2)/2,
            };

            if (!Directory.Exists("Temp"))
            {
                Directory.CreateDirectory("Temp");
            }


            //Create shares
            var shares = new ShareFile[9];
            for (var i = 0; i < 3; i++)
            {
                for (var j = 0; j < 3; j++)
                {
                    var shareName = ShareFileNameParser.BuildName(Path.Combine("Temp", Path.GetFileName(path)), i, j);
                    shares[(i*3) + j] = ShareFileFactory.Create(shareName);
                }
            }

            using (var sourceStream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                using (var reader = new BinaryReader(sourceStream))
                {
                    while (sourceStream.Position < sourceStream.Length)
                    {
                        var bytes = reader.ReadBytes(bufferLength);
                        var value = BigIntegerExtensions.ToBigIntegerFromLittleEndianUnsignedBytes(bytes);

                        var firstLevelShares = StandartAlgorithm.Split(value, N);

                        for (var i = 0; i < 3; i++)
                        {
                            var secondLevelShares = StandartAlgorithm.Split(firstLevelShares[i], secondLevelModules[i]);
                            for (var j = 0; j < 3; j++)
                            {
                                shares.SingleOrDefault(x => x.FirstLevelIndex == i && x.SecondLevelIndex == j)?
                                    .WriteBigInteger(secondLevelShares[j]);
                            }
                        }
                    }
                }
            }

            foreach (var shareFile in shares)
            {
                shareFile.Dispose();
            }
        }

    }
}
