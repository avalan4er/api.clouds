﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Api.Clouds.Common.Numerics;
using Api.Clouds.Crypto.Algorithms;
using Api.Clouds.Crypto.Share;
using NLog;

namespace Api.Clouds.Crypto.Processes
{
    public class ComposeProcess
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Собирает из папки Temp 2 пары парных долей и возвращает их в списке из четырех ShareFile
        /// </summary>
        /// <param name="sharesFileName">Название файла без номеров долей и расширения</param>
        /// <returns></returns>
        private List<ShareFile> AggregateLocalShareFiles(string sharesFileName)
        {
            var downloadedShares = Directory.GetFiles("Temp").Where(s => s.Contains(sharesFileName)).ToList();
            if (!downloadedShares.Any() || downloadedShares.Count() < 4)
            {
                return null;
            }

            var pairedFiles = new List<string>();
            for (int i = 0; i < 3; i++)
            {
                var pairs = downloadedShares.Where(x =>
                {
                    int firstLevelIndex, secondLevelIndex;
                    string temp;
                    ShareFileNameParser.ParceName(x, out firstLevelIndex, out secondLevelIndex, out temp);

                    return firstLevelIndex == i;
                }).Take(2);

                pairedFiles.AddRange(pairs);
            }

            return pairedFiles.Take(4).Select(ShareFileFactory.Create).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pathToSave">путь к сохраняемому файлу</param>
        /// <param name="sharesFileName">название оригинального файла без номеров долей и расширения</param>
        /// <param name="N">параметр разделения</param>
        public void PerformCompose(string pathToSave, string sharesFileName, int N)
        {
            var shares = AggregateLocalShareFiles(sharesFileName);
            if (shares == null || !shares.Any())
            {
                return;
            }

            using (var outStream = new FileStream(pathToSave, FileMode.Create, FileAccess.Write))
            {
                using (var writer = new BinaryWriter(outStream))
                {
                    while (shares[0].Stream.Position < shares[0].Stream.Length)
                    {
                        var shareA = StandartAlgorithm.Compose(shares[0].ReadBigInteger(), shares[0].SecondLevelIndex,
                            shares[1].ReadBigInteger(), shares[1].SecondLevelIndex, (N + 1)/2);
                        var shareB = StandartAlgorithm.Compose(shares[2].ReadBigInteger(), shares[2].SecondLevelIndex,
                            shares[3].ReadBigInteger(), shares[3].SecondLevelIndex,
                            (shares[2].FirstLevelIndex == 2) ? (N + 2)/2 : (N + 1)/2);


                        var secret = StandartAlgorithm.Compose(
                            shareA,
                            shares[0].FirstLevelIndex,
                            shareB,
                            shares[2].FirstLevelIndex,
                            N
                            );

                        writer.Write(secret.ToUnsignedLittleEndianBytes());
                    }
                }
            }

            foreach (var shareFile in shares)
            {
                shareFile.Dispose();
            }
        }

    }
}
