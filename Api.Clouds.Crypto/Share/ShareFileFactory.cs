﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Clouds.Crypto.Share
{
    public class ShareFileFactory
    {
        public static ShareFile Create(string path)
        {
            int firstLevelIndex;
            int secondLevelIndex;
            string fileNameWithoutIndex;

            ShareFileNameParser.ParceName(path, out firstLevelIndex, out secondLevelIndex, out fileNameWithoutIndex);

            var shareFile = new ShareFile()
            {
                FirstLevelIndex = firstLevelIndex,
                SecondLevelIndex = secondLevelIndex,
                Stream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite),
            };

            return shareFile;

        }
    }
}
