﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Api.Clouds.Common.Numerics;
using NLog;

namespace Api.Clouds.Crypto.Share
{
    public class ShareFile : IDisposable
    {
        public ShareFile()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
        }

        private readonly Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public int FirstLevelIndex { get; set; }
        public int SecondLevelIndex { get; set; }
        public Stream Stream { get; set; }

        private BinaryReader _reader;
        private BinaryWriter _writer;

        public BigInteger ReadBigInteger()
        {
            if (_reader == null)
            {
                _reader = new BinaryReader(Stream);
            }

            var lengthBuffer = new byte[4];
            _reader.Read(lengthBuffer, 0, 4);
            var length = BitConverter.ToInt32(lengthBuffer, 0);

            var bytes = _reader.ReadBytes(length);
            return BigIntegerExtensions.ToBigIntegerFromLittleEndianUnsignedBytes(bytes);
        }

        public void WriteBigInteger(BigInteger value)
        {
            if (_writer == null)
            {
                _writer = new BinaryWriter(Stream);
            }

            var outData = value.ToUnsignedLittleEndianBytes();
            var lengthBuffer = BitConverter.GetBytes(outData.Length);

            _writer.Write(lengthBuffer, 0, 4);
            _writer.Write(outData);
        }

        public void Dispose()
        {
            _reader?.Dispose();
            _writer?.Dispose();

            Stream?.Dispose();
        }
    }
}
