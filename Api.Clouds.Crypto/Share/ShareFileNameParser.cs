﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Clouds.Crypto.Share
{
    public class ShareFileNameParser
    {
        public static string BuildName(string path, int firstLevelIndex, int secondLevelIndex)
        {
            var fileDirectory = Path.GetDirectoryName(path);
            var fileName = Path.GetFileName(path);
            var extension = Path.GetExtension(path);

            return Path.Combine(fileDirectory, fileName + ".shr" + firstLevelIndex + secondLevelIndex);
        }

        public static void ParceName(string path, out int firstLevelIndex, out int secondLevelIndex, out string pathWithoutIndex)
        {
            var extIdx = path.IndexOf(".shr");
            if (extIdx != -1)
            {
                var fileName = path.Substring(0, extIdx);

                int.TryParse(path[path.Length - 2].ToString(), out firstLevelIndex);
                int.TryParse(path[path.Length - 1].ToString(), out secondLevelIndex);

                pathWithoutIndex = fileName;
            } else
            {
                pathWithoutIndex = path;
                firstLevelIndex = -1;
                secondLevelIndex = -1;
            }
        }

    }
}
