﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Api.Clouds.Common.Numerics;

namespace Api.Clouds.Crypto.Algorithms
{
    public class StandartAlgorithm 
    {
        public static BigInteger[] Split(BigInteger input, int n)
        {
            BigInteger share1, share2, share3;

            var temp = (BigInteger.One << n) - BigInteger.One;
            if (BigInteger.Compare(input, temp) > -1)
            {
                share2 = input & temp;
                share1 = share2 + (input >> n);

                if (BigInteger.Compare(temp, share1) != 1) { share1 = share1 - temp; }

                temp = (BigInteger.One << (n + 1)) - BigInteger.One;
                share3 = (temp & input) + (input >> (n + 1));
                if (BigInteger.Compare(temp, share3) < 1) { share3 = share3 - temp; }
            }
            else
            {
                share1 = input;
                share2 = input;
                share3 = input;
            }

            return new [] { share1,
                            share2,
                            share3 };
        }

        public static BigInteger Compose(BigInteger firstShare, int firstShareIndex, BigInteger secondShare, int secondShareIndex, int n)
        {
            if (firstShareIndex == 0)
            {
                if (secondShareIndex == 1)
                {
                    return ComposeS1S2(firstShare, secondShare, n);
                }
                else if (secondShareIndex == 2)
                {
                    return ComposeS1S3(firstShare, secondShare, n);
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
            else if (firstShareIndex == 1)
            {
                return ComposeS2S3(firstShare, secondShare, n);
            }
            else
            {
                throw new InvalidOperationException();
            }
        }
        private static BigInteger ComposeS1S2(BigInteger S1, BigInteger S2, int n)
        {
            BigInteger z = S1 - S2;
            if (z.Sign == -1) { z = z + ((BigInteger.One << n) - 1); }
            BigInteger result = (z << n) + S2;

            return result;
        } // Работает
        private static BigInteger ComposeS1S3(BigInteger S1, BigInteger S3, int n)
        {
            BigInteger P = (BigInteger.One << (2 * n + 1)) - (BigInteger.One << (n + 1)) - (BigInteger.One << n) + BigInteger.One;
            BigInteger result = ((S1 - S3) << (n + 1)) + 2 * S3 - S1;
            if (result.Sign == -1) { result = result + P; }
            //if (BigInteger.Compare(P, result) != 1) { result = result - P; }

            return result;
        } // Непонятно
        private static BigInteger ComposeS2S3(BigInteger S2, BigInteger S3, int n)
        {
            BigInteger Temp = S3 - S2;
            BigInteger result = (Temp << (n + 1)) + S2;
            if (result.Sign == -1) { result = result + (BigInteger.One << (2 * n + 1)) - (BigInteger.One << n); }
            //if (BigInteger.Compare((BigInteger.One << (2 * n + 1)) - (BigInteger.One << n), result) != 1) { result = result - ((BigInteger.One << (2 * n + 1)) - (BigInteger.One << n)); }

            return  result;
        } // Работает
    }
}
