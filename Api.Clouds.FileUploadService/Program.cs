﻿using Api.Clouds.ModuleBase;
using Autofac;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Api.Clouds.FileUploadService
{
    class Program
    {

        private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        [STAThread]
        static void Main(string[] args)
        {
            var modules = new List<IModule>();
            var types = ModuleInstanceFactory.GetExportedTypes().ToList();
            types.ForEach(t => modules.Add(ModuleInstanceFactory.CreateInstance(t.ToString())));

            foreach (var module in modules)
            {
                try
                {
                    module.Authenticate();
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Ошибка авторизации");
                }
            }

            var uploader = new Uploader(modules);
            var timer = new Timer()
            {
                Interval = 10000
            };
            timer.Elapsed += (s, e) =>
            {
                uploader.Upload();
            };

            timer.Start();

            Console.ReadKey();
        }

    }

    
}
