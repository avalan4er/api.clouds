﻿using System.IO;
using System.Windows;

namespace Api.Clouds.Dialogs
{
    /// <summary>
    /// Interaction logic for AboutWindow.xaml
    /// </summary>
    public partial class AboutWindow : Window
    {
        public AboutWindow()
        {
            InitializeComponent();

            if (File.Exists("Resources/AboutContent.txt"))
            {
                var AboutInfo = File.ReadAllText("Resources/AboutContent.txt");
                this.AboutContent.Text = AboutInfo;
            }
            else
            {
                this.AboutContent.Text = "Файл сведений о программе не найден...";
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    }
}
