﻿using Api.Clouds.ModuleBase;
using Caliburn.Micro;
using System.Collections.Generic;

namespace Api.Clouds.ViewModels
{
    public class SplitSettingsViewModel : Screen
    {
        #region Properties
        public BindableCollection<IModule> Modules { get; set; }
        public BindableCollection<IModule> ModulesToUpload { get; set; }
        public BindableCollection<IModule> ModulesToDownload { get; set; }

        private int _N;
        public int N 
        {
            get
            {
                return _N;
            }
            set
            {
                if (_N == value) return;
                _N = value;
                NotifyOfPropertyChange(() => N);
            }
        }
        #endregion

        public SplitSettingsViewModel(IEnumerable<IModule> AvalibleModules, int N)
        {
            Modules = new BindableCollection<IModule>(AvalibleModules);
            ModulesToUpload = new BindableCollection<IModule>();
            ModulesToDownload = new BindableCollection<IModule>();
            this.N = N;
        }

        public void AddModuleToUpload(IModule module)
        {
            if (module != null && !ModulesToUpload.Contains(module))
            {
                ModulesToUpload.Add(module);
            }
        }
        public void RemoveModuleToUpload(IModule module)
        {
            if (module != null)
            {
                ModulesToUpload.Remove(module);
            }
        }

        #region Compose module list
        public void AddModuleToDownload(IModule module)
        {
            if (module != null && !ModulesToDownload.Contains(module))
            {
                ModulesToDownload.Add(module);
            }
        }
        public void RemoveModuleToDownload(IModule module)
        {
            if (module != null)
            {
                ModulesToDownload.Remove(module);
            }
        }
        #endregion
    }
}
