﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Api.Clouds.Crypto.Processes;
using Api.Clouds.Crypto.Share;
using Api.Clouds.Extensions;
using Api.Clouds.ModuleBase;
using Api.Clouds.Views;
using Autofac;
using Caliburn.Micro;
using NLog;
using LogManager = NLog.LogManager;
using Api.Clouds.Uploader;
using Microsoft.Win32;
using System.Diagnostics;
using System.Threading;

/// <summary>
/// TODO: Избавиться от BigInteger в пользу математических операций над массивами байт (попробовать реализовать параллельные алгоритмы)
/// </summary>

namespace Api.Clouds.ViewModels
{
    class ShellViewModel : Screen
    {
        private readonly ILifetimeScope _lifetimeScope;
        private List<FileStream> _activeStreams;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly string[][] _sharingMap = { new[] { "02" },
                                                    new[] { "00", "12" },
                                                    new[] { "01", "11", "21" },
                                                    new[] { "10", "22" },
                                                    new[] { "20" }};
        private readonly string _tempDirectoryPath = "Temp";
        private CancellationTokenSource cts;

        private List<string> _CloudFilesList;
        struct ModuleFilepair
        {
            public IModule Module;
            public string FileName;
        }

        public ShellViewModel(ILifetimeScope lifetimeScope)
        {
            _lifetimeScope = lifetimeScope;
            Modules = new BindableCollection<IModule>();
            ModulesToUpload = new BindableCollection<IModule>();
            ModulesToDownload = new BindableCollection<IModule>();
            _CloudFilesList = new List<string>();
            CloudFilesList = new BindableCollection<string>();

            var types = ModuleInstanceFactory.GetExportedTypes();
            foreach (var type in types)
            {
                Modules.Add(
                    ModuleInstanceFactory.CreateInstance(type.ToString())
                    );
            }

            if (Modules.Count >= 5)
            {
                for (int i = 0; i < 5; i++)
                {
                    ModulesToUpload.Add(Modules[i]);
                }
                for (int i = 0; i < 3; i++)
                {
                    if (i == 2)
                    {
                        ModulesToDownload.Add(Modules[i + 1]);
                        continue;
                    }
                    ModulesToDownload.Add(Modules[i]);
                }
            }
            _activeStreams = new List<FileStream>();
        }

        #region Properties
        #region Private
        private int _n = 2048;
        private string _filePath;
        private string _resultOutput = "";
        private string _NameOfFileToDownload;
        private string _NameOfFileToCompose;
        private bool _SplitOperationStarted;
        private bool _LocalSplitOperationComplete;
        private int _UploadedFilesCounts;
        private bool _UploadProcessComplete;
        private int _DownloadedFilesCount;
        private bool _DownloadProcessStarted;
        private bool _DownloadProcessComplete;
        private bool _ComposeProcessStarted;
        private bool _ComposeProcessComplete;
        #endregion

        #region Public
        public BindableCollection<IModule> Modules { get; set; }
        public int ModuleN
        {
            get
            {
                return _n;
            }
            set
            {
                if (_n == value) return;
                _n = value;
                NotifyOfPropertyChange(() => ModuleN);
            }
        }
        public BindableCollection<IModule> ModulesToUpload { get; set; }
        public BindableCollection<IModule> ModulesToDownload { get; set; }
        public BindableCollection<string> CloudFilesList { get; set; }
        public string ResultOutput
        {
            get
            {
                return _resultOutput;
            }
            set
            {
                if (_resultOutput == value) return;
                _resultOutput = value;
                NotifyOfPropertyChange(() => ResultOutput);
            }
        }
        public string NameOfFileToCompose
        {
            get
            {
                return _NameOfFileToCompose;
            }
            set
            {
                if (_NameOfFileToCompose == value) return;
                _NameOfFileToCompose = value;
                NotifyOfPropertyChange(() => NameOfFileToCompose);
            }
        }

        private StreamTaskContainer _uploadContainer;
        public StreamTaskContainer UploadContainer
        {
            get
            {
                return _uploadContainer;
            }
            set
            {
                if (_uploadContainer != value)
                {
                    _uploadContainer = value;
                    NotifyOfPropertyChange(() => _uploadContainer);
                }
            }
        }

        private StreamTaskContainer _downloadContainer;
        public StreamTaskContainer DownloadContainer
        {
            get
            {
                return _downloadContainer;
            }
            set
            {
                if (_downloadContainer != value)
                {
                    _downloadContainer = value;
                    NotifyOfPropertyChange(() => DownloadContainer);
                }
            }
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get
            {
                return _isBusy;
            }
            set
            {
                if(_isBusy != value)
                {
                    _isBusy = value;
                    NotifyOfPropertyChange(() => IsBusy);
                }
            }
        }

        #region Visual states
        public bool SplitOperationStarted
        {
            get
            {
                return _SplitOperationStarted;
            }
            set
            {
                if (_SplitOperationStarted == value) return;
                _SplitOperationStarted = value;
                NotifyOfPropertyChange(() => SplitOperationStarted);
            }
        }
        public bool LocalSplitOperationComplete
        {
            get
            {
                return _LocalSplitOperationComplete;
            }
            set
            {
                if (_LocalSplitOperationComplete == value) return;
                _LocalSplitOperationComplete = value;
                NotifyOfPropertyChange(() => LocalSplitOperationComplete);
            }
        }
        public int UploadedFilesCount
        {
            get
            {
                return _UploadedFilesCounts;
            }
            set
            {
                if (_UploadedFilesCounts == value) return;
                _UploadedFilesCounts = value;
                NotifyOfPropertyChange(() => UploadedFilesCount);
            }
        }
        public bool UploadProcessComplete
        {
            get
            {
                return _UploadProcessComplete;
            }
            set
            {
                if (_UploadProcessComplete == value) return;
                _UploadProcessComplete = value;
                NotifyOfPropertyChange(() => UploadProcessComplete);
            }
        }
        public int DownloadedFilesCount
        {
            get
            {
                return _DownloadedFilesCount;
            }
            set
            {
                if (_DownloadedFilesCount == value) return;
                _DownloadedFilesCount = value;
                NotifyOfPropertyChange(() => DownloadedFilesCount);
            }
        }
        public bool DownloadProcessStarted
        {
            get
            {
                return _DownloadProcessStarted;
            }
            set
            {
                if (_DownloadProcessStarted == value) return;
                _DownloadProcessStarted = value;
                NotifyOfPropertyChange(() => DownloadProcessStarted);
            }
        }
        public bool DownloadProcessComplete
        {
            get
            {
                return _DownloadProcessComplete;
            }
            set
            {
                if (_DownloadProcessComplete == value) return;
                _DownloadProcessComplete = value;
                NotifyOfPropertyChange(() => DownloadProcessComplete);
            }
        }
        public bool ComposeProcessStarted
        {
            get
            {
                return _ComposeProcessStarted;
            }
            set
            {
                if (_ComposeProcessStarted == value) return;
                _ComposeProcessStarted = value;
                NotifyOfPropertyChange(() => ComposeProcessStarted);
            }
        }
        public bool ComposeProcessComplete
        {
            get
            {
                return _ComposeProcessComplete;
            }
            set
            {
                if (_ComposeProcessComplete == value) return;
                _ComposeProcessComplete = value;
                NotifyOfPropertyChange(() => ComposeProcessComplete);
            }
        }
        #endregion
        #endregion
        #endregion

        #region Commands
        public async Task CancelTask()
        {
            if(cts != null)
            {
                cts.Cancel();
            }
        }

        public async Task Authenticate(string ModuleName)
        {
            var module = Modules.SingleOrDefault(x => x.ModuleName == ModuleName);
            if (module == null)
            {
                _logger.Error("ShellViewModel: module not loaded.");
                return;
            }

            try
            {
                await module.Authenticate();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Ошибка авторизации в модуле: {0}", module.ModuleName);
            }
        }

        public async Task FileDropped(DragEventArgs e)
        {
            IsBusy = true;
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                // Note that you can have more than one file.
                var files = (string[])e.Data.GetData(DataFormats.FileDrop);

                if (files != null && files.Any())
                {
                    cts = new CancellationTokenSource();

                    foreach (var file in files)
                    {
                        try
                        {
                            Directory.Delete(_tempDirectoryPath, true);
                            Directory.CreateDirectory(_tempDirectoryPath);
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex, "Ошибка удаления папки");
                        }

                        var sp = new SplitProcess();

                        var sw1 = Stopwatch.StartNew();
                        var sw = Stopwatch.StartNew();
                        sp.PerformSplit(file, ModuleN);
                        sw.Stop();

                        UploadContainer = new StreamTaskContainer();
                        for (int i = 0; i < _sharingMap.Length; i++)
                        {
                            var uploadingModule = Modules.ElementAt(i);
                            for (int j = 0; j < _sharingMap[i].Length; j++)
                            {
                                cts.Token.ThrowIfCancellationRequested();
                                var uploadingFileName = Path.Combine(_tempDirectoryPath, Path.GetFileName(file + ".shr" + _sharingMap[i][j]));
                                var task = new StreamTask(uploadingFileName, uploadingModule);
                                UploadContainer.Add(task);
                                task.Upload();
                            }
                        }
                        await UploadContainer.WaitAll(cts.Token);
                        sw1.Stop();

                        var reportString = "Отчет:\n";
                        reportString += $"Файл '{file}' разделен за {sw.Elapsed.TotalSeconds} секунд.\n\n";
                        foreach (var task in UploadContainer.Tasks)
                        {
                            reportString += $"Загрузка файла '{task.LoadProcess.FileInfo.Name}' размером {task.LoadProcess.FileInfo.Length / 1024}кб с помощью модуля '{task.Loader.ModuleName}' заняла {task.LoadProcess.ElapsedTime.TotalSeconds}сек (скорость {task.LoadProcess.GetSpeed()} кб/с). \n";
                        }
                        reportString += $"Общее время операции {sw1.Elapsed.TotalSeconds} сек.";

                        var reportBox = new ReportBoxView() { DataContext = new ReportBoxViewModel() { ReportString = reportString } };
                        reportBox.Show();
                    }
                }
            }
            IsBusy = false;
        }

        public async Task SaveFile(string fileName)
        {
            _logger.Info($"Начало операции сохранения файла '{fileName}'");
            IsBusy = true;
            cts = new CancellationTokenSource();

            List<Tuple<IModule, string>> filesToDownload = new List<Tuple<IModule, string>>();
            List<Tuple<IModule, string>> modulesFiles = null;

            await Task.Run(() =>
            {
                modulesFiles = Modules.Where(x => x.IsAuthenticated).SelectMany(x => x.GetFiles().Select(y => new Tuple<IModule, string>(x, y))).Where(x => x.Item2 != null && x.Item2.Contains(fileName)).ToList();
                if (modulesFiles == null)
                {
                    _logger.Error("Ошибка при запросе списка всех файлов в облаках");
                    return;
                }
            });

            _logger.Info($"Запрошен список всех файлов в облаках");

            //Формирование списка файлов для загрузки
            for (int i = 0; i < 3; i++)
            {
                var pair = modulesFiles.Where(x =>
                {
                    int firstIndex, secondIndex;
                    string normalName;

                    ShareFileNameParser.ParceName(x.Item2, out firstIndex, out secondIndex, out normalName);
                    if (firstIndex == i) return true;

                    return false;
                });

                if (pair.Count() >= 2)
                {
                    filesToDownload.AddRange(pair.Take(2));
                    if (filesToDownload.Count == 4)
                    {
                        break;
                    }
                }
            }

            _logger.Info("Составлены пары для восстановления файла");

            // Очистка папки Temp
            try
            {
                Directory.Delete(_tempDirectoryPath, true);
                Directory.CreateDirectory(_tempDirectoryPath);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Ошибка удаления папки");
            }

            _logger.Info("Очищена папка для загрузки");

            var sw1 = Stopwatch.StartNew();

            _logger.Info("Начало загрузки файлов");

            // Загрузка файлов
            DownloadContainer = new StreamTaskContainer();
            foreach (var tuple in filesToDownload)
            {
                var task = new StreamTask(tuple.Item2, tuple.Item1);
                DownloadContainer.Add(task);
                task.Download();
            }

            await DownloadContainer.WaitAll(cts.Token);

            _logger.Info("Файлы загружены");

            Stopwatch sw = null;

            var saveDialog = new SaveFileDialog();
            if (saveDialog.ShowDialog() == true)
            {
                var cp = new ComposeProcess();
                var outFileName = saveDialog.FileName;
                sw = Stopwatch.StartNew();
                cp.PerformCompose(outFileName, Path.GetFileNameWithoutExtension(modulesFiles.First().Item2), ModuleN);
                sw.Stop();
            }

            sw1.Stop();

            _logger.Info("Файл сохранен");

            var reportString = "Отчет:\n";
            reportString += $"Файл '{saveDialog.FileName}' собран за {sw?.Elapsed.TotalSeconds} секунд.\n\n";
            foreach (var task in DownloadContainer.Tasks)
            {
                reportString += $"Загрузка файла '{task.LoadProcess.FileInfo.Name}' размером {task.LoadProcess.FileInfo.Length / 1024}кб с помощью модуля '{task.Loader.ModuleName}' заняла {task.LoadProcess.ElapsedTime.TotalSeconds}сек (скорость {task.LoadProcess.GetSpeed()} кб/с). \n";
            }
            reportString += $"Общее время операции {sw1.Elapsed.TotalSeconds} сек.";

            var reportBox = new ReportBoxView() { DataContext = new ReportBoxViewModel() { ReportString = reportString } };
            reportBox.Show();

            IsBusy = false;
        }

        public async Task UpdateFilesInCloudsList()
        {
            await Task.Run(() =>
            {
                _CloudFilesList.Clear();
                Parallel.ForEach(Modules, x =>
                {
                    if (!x.IsAuthenticated) return;
                    _CloudFilesList.AddRange(x.GetFiles().Distinct());
                });
                AggregateAvalibleFiles();
            });
        }

        public void OpenSplitSettingsDialog()
        {
            var dialog = new SplitSettingsViewModel(Modules, _n);
            dialog.ModulesToDownload.AddRange(this.ModulesToDownload);
            dialog.ModulesToUpload.AddRange(this.ModulesToUpload);
            var dialogView = new SplitSettingsView() { DataContext = dialog };
            dialogView.ShowDialog();
            ModulesToUpload.Clear();
            ModulesToUpload.AddRange(dialog.ModulesToUpload);
            ModulesToDownload.Clear();
            ModulesToDownload.AddRange(dialog.ModulesToDownload);
            _n = dialog.N;
        }


        #region Utils

        /// <summary>
        /// Обрабатывает список всех файлов в облаках и возвращает список файлов, которые возможно восстановить
        /// </summary>
        private void AggregateAvalibleFiles()
        {
            var tmpList = new List<string>();
            _CloudFilesList.ForEach(x =>
            {
                if (x != null && x.Contains(".shr"))
                {
                    var name = x.Substring(0, x.IndexOf(".shr"));
                    tmpList.Add(name);
                }
            });

            CloudFilesList.Clear();
            foreach (var name in tmpList)
            {
                var nameCount = tmpList.Where(x => x.Equals(name)).Count();
                if (nameCount >= 4 && !CloudFilesList.Contains(name))
                {
                    CloudFilesList.Add(name);
                }
            }
        }

        #endregion

      
        #endregion
    }


}
