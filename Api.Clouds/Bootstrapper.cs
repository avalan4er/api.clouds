﻿using System;
using System.Windows;
using Api.Clouds.ViewModels;
using Autofac;
using Caliburn.Micro;
using NLog;
using LogManager = NLog.LogManager;

namespace Api.Clouds
{
    class Bootstrapper : BootstrapperBase
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private static IContainer _container;

        public Bootstrapper()
        {
            Initialize();
        }

        protected override void Configure()
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.Register<IWindowManager>(_ => new WindowManager()).SingleInstance();
            builder.Register<IEventAggregator>(_ => new EventAggregator()).SingleInstance();
            builder.RegisterType<ShellViewModel>().AsSelf().SingleInstance();

            _container = builder.Build();

            base.Configure();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            _logger.Info("App startup");
            DisplayRootViewFor<ShellViewModel>();
            base.OnStartup(sender, e);
        }

        protected sealed override object GetInstance(Type serviceType, string key)
        {
            _logger.Trace("Get instance of type {0}{1}.", serviceType.FullName,
                String.IsNullOrWhiteSpace(key) ? String.Empty : " with key " + key);

            object instance = null;
            // Try to resolve a service by key and type.
            if (!String.IsNullOrWhiteSpace(key) && _container.TryResolveNamed(key, serviceType, out instance))
            {
                return instance;
            }
            // Try to resolve a service by type.
            if (_container.TryResolve(serviceType, out instance))
            {
                return instance;
            }
            throw new Exception(String.Format("Could not locate any instances of contract {0}.", key ?? serviceType.Name));
        }
    }
}
