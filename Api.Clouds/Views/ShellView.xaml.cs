﻿using Api.Clouds.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Api.Clouds.Views
{
    /// <summary>
    /// Interaction logic for ShellView.xaml
    /// </summary>
    public partial class ShellView : Window
    {
        public ShellView()
        {
            InitializeComponent();
            WindowHeader.MouseLeftButtonDown += delegate { this.DragMove(); };
        }
        public void MinimizeButton_Click(object sender, EventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }
        public void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void AboutButton_Click(object sender, EventArgs e)
        {
            var dialog = new AboutWindow();
            dialog.ShowDialog();
        }

        private void Border_Drop(object sender, DragEventArgs e)
        {

        }
    }
}
