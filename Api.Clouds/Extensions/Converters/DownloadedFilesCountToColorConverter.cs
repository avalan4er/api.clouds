﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Api.Clouds.Extensions.Converters
{
    public class DownloadedFilesCountToColorConverter : IValueConverter
    {
        private string[] colors = new string[5] 
        {
            "#D24826",
            "#8C6519",
            "#46830C",
            "#179704",
            "#00A100"
        };

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int count = ((int)value);
            return colors[count];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
