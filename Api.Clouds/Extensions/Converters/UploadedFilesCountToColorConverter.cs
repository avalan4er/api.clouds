﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Api.Clouds.Extensions.Converters
{
    public class UploadedFilesCountToColorConverter : IValueConverter
    {
        private string[] colors = new string[10] 
        {
            "#D24826",
            "#BA5121",
            "#A35B1D",
            "#8C6519",
            "#746F15",
            "#5D7910",
            "#46830C",
            "#2E8D08",
            "#179704",
            "#00A100"
        };

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int count = ((int)value);
            return colors[count];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
