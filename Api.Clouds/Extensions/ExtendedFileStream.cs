﻿using System.IO;

namespace Api.Clouds.Extensions
{
    class ExtendedFileStream : FileStream
    {
        public ExtendedFileStream(string path, int firstIndex, int secondIndex)
            : base(path, FileMode.Open, FileAccess.Read)
        {
            FirstIndex = firstIndex;
            SecondIndex = secondIndex;
        }

        public int FirstIndex { get; set; }
        public int SecondIndex { get; set; }
    }
}
