﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Api.Clouds.Modules.Box
{
    /// <summary>
    /// Interaction logic for LoginScreen.xaml
    /// </summary>
    public partial class LoginScreen : Window
    {
        private Uri AuthUri;
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        public LoginScreen()
        {
            InitializeComponent();
        }

        public LoginScreen(Uri uri)
            : this()
        {
            this.AuthUri = uri;
        }

        private void Browser_Loaded(object sender, RoutedEventArgs e)
        {
            Browser.Navigating += Browser_Navigating;
            Browser.Navigate(AuthUri.AbsoluteUri);
        }

        void Browser_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            if (e.Uri.AbsoluteUri.Contains("https://youcantgetthisninjas.com") && !e.Uri.AbsoluteUri.Contains("app.box.com"))
            {
                var responseParams = e.Uri.AbsoluteUri.Split('?')[1].Split('&').Select(x => x.Split('=')).Select(x => new { Key = x[0], Value = x[1] }).ToDictionary(x => x.Key, x => x.Value);
                var authCode = responseParams.SingleOrDefault(x => x.Key == "code" || x.Key == "ei").Value;
                if (AuthComplete != null && authCode != null)
                {
                    AuthComplete(this, authCode);
                }
            }
        }

        public EventHandler<string> AuthComplete { get; set; }
    }
}
