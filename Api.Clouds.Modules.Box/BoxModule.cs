﻿using Api.Clouds.ModuleBase;
using Api.Clouds.ModuleBase.Utils;
using Box.V2;
using Box.V2.Auth;
using Box.V2.Config;
using Box.V2.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Api.Clouds.Modules.Box
{
    [Export(typeof(IModule)), PartCreationPolicy(CreationPolicy.NonShared)]
    [ExportModule(ModuleName = "Box", AuthenticationType = AuthenticationType.OAuth)]
    public class BoxModule : IModule, INotifyPropertyChanged
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private BoxClient _client;
        private OAuthSession _session;
        private Thread _sessionMaintainer;

        #region Properties

        private bool _isAuthenticated;
        public string IconName => AppDomain.CurrentDomain.BaseDirectory + @"Resources\Images\box-icon.png";

        public bool IsAuthenticated
        {
            get
            {
                return _isAuthenticated;
            }
            set
            {
                if (_isAuthenticated == value) return;
                _isAuthenticated = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsAuthenticated"));
            }
        }

        public string ModuleName => "Box";

        #endregion Properties

        #region Sync

        public LoadProcess Download(Stream stream, string cloudFileName)
        {
            if (!IsAuthenticated)
            {
                _logger.Error("Module: Box; Method: Download; Errror: Not authenticated. File name: " + cloudFileName);
                return null;
            }

            var downloadProcess = new LoadProcess();

            _logger.Trace("Module: Box; Method: Download; Download process with Id " + downloadProcess.Id + "started. File name: " + cloudFileName);

            var folderItems = _client.FoldersManager.GetFolderItemsAsync("0", 100).Result;
            var file = folderItems.Entries.SingleOrDefault(x => x.Name.Equals(cloudFileName));
            if (file == null)
            {
                _logger.Error("Module: Box; Method: Download; File not found. File name: " + cloudFileName);
                return null;
            }

            var watch = Stopwatch.StartNew();

            try
            {
                (_client.FilesManager.DownloadStreamAsync(file.Id).Result).CopyTo(stream);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Module: Box; Method: Download; File name: {0}", cloudFileName);
                return null;
            }
            finally
            {
                watch.Stop();
                downloadProcess.ElapsedTime = watch.Elapsed;
            }

            downloadProcess.FileInfo = new FileInfo((stream as FileStream)?.Name);

            _logger.Trace("Module: Box; Method: Download; Download process with Id " + downloadProcess.Id + " complete. File name: " + cloudFileName);

            return downloadProcess;
        }

        /// <summary>
        /// Синхронная загрузка файла
        /// </summary>
        /// <param name="localFileName">Поток для записи файла</param>
        /// <param name="cloudFileName">Имя файла</param>
        public LoadProcess Download(string localFileName, string cloudFileName)
        {
            if (!IsAuthenticated)
            {
                _logger.Error("Module: Box; Method: Download; Errror: Not authenticated. File name: " + cloudFileName);
                return null;
            }

            var downloadProcess = new LoadProcess();

            _logger.Trace("Module: Box; Method: Download; Download process with Id " + downloadProcess.Id + "started. File name: " + cloudFileName);

            var folderItems = _client.FoldersManager.GetFolderItemsAsync("0", 100).Result;
            var file = folderItems.Entries.SingleOrDefault(x => x.Name.Equals(cloudFileName));
            if (file == null)
            {
                _logger.Error("Module: Box; Method: Download; File not found. File name: " + cloudFileName);
                return null;
            }

            using (var fileStream = new FileStream(localFileName, FileMode.Create, FileAccess.Write))
            {
                var watch = Stopwatch.StartNew();

                try
                {
                    (_client.FilesManager.DownloadStreamAsync(file.Id).Result).CopyTo(fileStream);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Module: Box; Method: Download; File name: {0}", cloudFileName);
                    return null;
                }
                finally
                {
                    watch.Stop();
                    downloadProcess.ElapsedTime = watch.Elapsed;
                }
            }

            downloadProcess.FileInfo = new FileInfo(localFileName);
            _logger.Trace("Module: Box; Method: Download; Download process with Id " + downloadProcess.Id + " complete. File name: " + localFileName);

            return downloadProcess;

            //_logger.Info("Box module: File download start. FileName: " + fileName);
            //if (!IsAuthenticated)
            //{
            //    _logger.Warn("Box module: File download canceled. Not authenticated!");
            //    return;
            //}

            //var folderItems = Client.FoldersManager.GetFolderItemsAsync("0", 50).Result;
            //var file = folderItems.Entries.SingleOrDefault(x => x.Name.Equals(fileName));
            //if (file == null)
            //{
            //    _logger.Warn("Box module: File download canceled. File not found!");
            //    return;
            //} else
            //{
            //    try
            //    {
            //        (Client.FilesManager.DownloadStreamAsync(file.Id).Result).CopyTo(outStream);
            //    } catch (Exception ex)
            //    {
            //        _logger.Error("Box module: File download error!", ex);
            //        return;
            //    }
            //}
        }

        /// <summary>
        /// Запрос списка файлов
        /// </summary>
        /// <returns>список имен файлов</returns>
        public List<string> GetFiles()
        {
            if (!IsAuthenticated)
            {
                _logger.Warn("Module: Box; Method: GetFiles; Errror: Not authenticated.");
                return null;
            }

            List<BoxItem> folderItems;
            try
            {
                folderItems = _client.FoldersManager.GetFolderItemsAsync("0", 50).Result.Entries;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Module: Box; Method: GetFiles; ");
                return null;
            }

            List<string> result = new List<string>();
            folderItems.ForEach(x => result.Add(x.Name));

            return result;
        }

        public LoadProcess Upload(Stream stream, string cloudFileName)
        {
            if (!IsAuthenticated)
            {
                _logger.Error("Module: Box; Method: Upload; Errror: Not authenticated. File name: " + cloudFileName);
                return null;
            }
            if (stream == Stream.Null)
            {
                _logger.Error("stream is null");
            }

            var uploadProcess = new LoadProcess()
            {
                FileInfo = new FileInfo((stream as FileStream)?.Name),
            };
            _logger.Trace("Module: Box; Method: Upload; Upload process with Id " + uploadProcess.Id.ToString() + " started. File name:" + cloudFileName);

            BoxFileRequest request = new BoxFileRequest()
            {
                Name = cloudFileName,
                Parent = new BoxRequestEntity() { Id = "0" },
            };

            // Удаление старого файла если он был
            var folderItems = _client.FoldersManager.GetFolderItemsAsync("0", 50).Result;
            var oldFile = folderItems.Entries.SingleOrDefault(x => x.Name.Equals(cloudFileName));
            if (oldFile != null)
            {
                var deleted = _client.FilesManager.DeleteAsync(oldFile.Id).Result;
                if (deleted != true)
                {
                    _logger.Error("Module: Box; Method: Upload; Delete file failed. File name: " + cloudFileName);
                    return null;
                }
            }

            var watch = Stopwatch.StartNew();
            try
            {
                var boxFile = _client.FilesManager.UploadAsync(request, stream).Result;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Module: Box; Method: Upload; Delete file failed. File name: {0}", cloudFileName);
            }
            finally
            {
                watch.Stop();
                uploadProcess.ElapsedTime = watch.Elapsed;
            }
            _logger.Trace("Module: Box; Method: Upload; Upload process with Id {0} complete. File name: {1}", uploadProcess.Id, cloudFileName);

            return uploadProcess;
        }

        /// <summary>
        /// Синхронная выгрузка файла
        /// </summary>
        /// <param name="localFileName">Имя и путь файла для сохранения</param>
        /// <param name="cloudFileName">Имя файла в облаке</param>
        public LoadProcess Upload(string localFileName, string cloudFileName)
        {
            if (!IsAuthenticated)
            {
                _logger.Error("Module: Box; Method: Upload; Errror: Not authenticated. File name: " + localFileName);
                return null;
            }

            if (!File.Exists(localFileName))
            {
                _logger.Error("Module: Box; Method: Upload; Errror: File not exists. File name: " + localFileName);
                return null;
            }

            using (var fileStream = new FileStream(localFileName, FileMode.Open, FileAccess.Read))
            {
                return Upload(fileStream, cloudFileName);
            }
        }

        #endregion Sync

        #region Async

        public async Task<LoadProcess> DownloadFileAsync(Stream stream, string cloudFileName)
        {
            return await Task.Run(() => Download(stream, cloudFileName));
        }

        /// <summary>
        /// Асинхронная загрузка файла
        /// </summary>
        public async Task<LoadProcess> DownloadFileAsync(string localFileName, string cloudFileName)
        {
            return await Task.Run(() => Download(localFileName, cloudFileName));

            //if (fileName == null)
            //    throw new ArgumentException("fileName");
            //if (!IsAuthenticated)
            //{
            //    _logger.Warn("Box module: Async download canceled. Not authenticated!");
            //    if (this.DownloadProgressComplete != null)
            //    {
            //        this.DownloadProgressComplete(this, new OperationResult() { Result = OperationResultEnum.ERROR, FileName = fileName });
            //    }
            //    return;
            //}

            //var folderItems = await Client.FoldersManager.GetFolderItemsAsync("0", 50);
            //var file = folderItems.Entries.SingleOrDefault(x => x.Name.Equals(fileName));
            //if (file == null)
            //{
            //    _logger.Warn("Box module: Async download failed. File not found!");
            //    if (this.DownloadProgressComplete != null)
            //    {
            //        this.DownloadProgressComplete(this, new OperationResult() { Result = OperationResultEnum.ERROR, FileName = fileName });
            //    }
            //    return;
            //}

            //try
            //{
            //    (await Client.FilesManager.DownloadStreamAsync(file.Id)).CopyTo(outStream);
            //} catch (Exception ex)
            //{
            //    _logger.Error("Box module: Async download failed.", ex);
            //    if (this.DownloadProgressComplete != null)
            //    {
            //        this.DownloadProgressComplete(this, new OperationResult() { Result = OperationResultEnum.ERROR, FileName = fileName });
            //    }
            //    return;
            //}

            //_logger.Info("Box module: Async download complete.");
            //if (this.DownloadProgressComplete != null)
            //{
            //    this.DownloadProgressComplete(this, new OperationResult() { Result = OperationResultEnum.OK, FileName = fileName });
            //}
        }

        public async Task<LoadProcess> UploadFileAsync(Stream stream, string cloudFileName)
        {
            return await Task.Run(() => Upload(stream, cloudFileName));
        }

        /// <summary>
        /// Асинхронная выгрузка файла
        /// </summary>
        /// <param name="localFileName"></param>
        /// <param name="cloudFileName"></param>
        public async Task<LoadProcess> UploadFileAsync(string localFileName, string cloudFileName)
        {
            return await Task.Run(() => Upload(localFileName, cloudFileName));

            //_logger.Info("Box module: Async upload started. FileName: " + fileName);
            //if (!IsAuthenticated)
            //{
            //    _logger.Warn("Box module: Async upload canceled. Not authenticated!");
            //    if (this.UploadProgressComplete != null)
            //    {
            //        this.UploadProgressComplete(this, new OperationResult() { Result = OperationResultEnum.ERROR, FileName = fileName });
            //    }
            //    return;
            //}

            //BoxFileRequest request = new BoxFileRequest() //Создаем запрос
            //{
            //    Name = fileName,
            //    Parent = new BoxRequestEntity() { Id = "0" },
            //};

            //var folderItems = await Client.FoldersManager.GetFolderItemsAsync("0", 50); //Запрашиваем список файлов с сервера
            //var oldFile = folderItems.Entries.SingleOrDefault(x => x.Name.Equals(fileName)); //Проверяем, занято ли имя
            //if (oldFile != null)
            //{
            //    var deleted = await Client.FilesManager.DeleteAsync(oldFile.Id);
            //    if (!deleted)
            //    {
            //        _logger.Warn("Box module: Async upload canceled. File exists, but not deleted. FileName: " + fileName);
            //        if (this.UploadProgressComplete != null)
            //        {
            //            this.UploadProgressComplete(this, new OperationResult() { Result = OperationResultEnum.ERROR, FileName = fileName });
            //        }
            //        return;
            //    }
            //}

            //try
            //{
            //    BoxFile boxFile = await Client.FilesManager.UploadAsync(request, stream);
            //} catch (Exception ex)
            //{
            //    _logger.Error("Box module: Async upload failed!", ex);
            //    if (this.UploadProgressComplete != null)
            //    {
            //        this.UploadProgressComplete(this, new OperationResult() { Result = OperationResultEnum.ERROR, FileName = fileName });
            //    }
            //}

            //_logger.Info("Box module: Async upload complete! FileName: " + fileName);
            //if (this.UploadProgressComplete != null)
            //{
            //    this.UploadProgressComplete(this, new OperationResult() { Result = OperationResultEnum.OK, FileName = fileName });
            //}
        }

        #endregion Async

        #region Auth

        /// <summary>
        /// Запуск процесса авторизации
        /// </summary>
        /// <returns></returns>
        public async Task Authenticate()
        {
            await Task.Run(() =>
            {
                _logger.Trace("Module: Box; Method: Authenticate; Authentication start.");
                var config = new BoxConfig("od301ujquxljs8grwvse15gf1tvem91r", "CffDanRRe5OwthHM6EdWAvID79QhDCRD", new Uri("https://youcantgetthisninjas.com"));
                _client = new BoxClient(config);
                _logger.Trace("Module: Box; Method: Authenticate; Client created.");

                Application.Current.Dispatcher.Invoke(() =>
                {
                    var ls = new LoginScreen(config.AuthCodeUri);
                    ls.AuthComplete += AuthCodeReceived;
                    _logger.Trace("Module: Box; Method: Authenticate; Login screen show.");
                    ls.Show();
                });
            });
        }

        /// <summary>
        /// Not implemented
        /// </summary>
        /// <param name="accountInfo"></param>
        /// <returns></returns>
        public async Task Authenticate(AccountInfo accountInfo)
        {
            await Authenticate();
        }

        private async void AuthCodeReceived(object sender, string authCode)
        {
            _logger.Trace("Module: Box; Method: AuthCodeRecieved; Code recieved. Code: {0}", authCode);

            try
            {
                _logger.Trace("Module: Box; Method: AuthCodeReciceved; Creating session.");
                _session = await _client.Auth.AuthenticateAsync(authCode);
                _logger.Trace("Module: Box; Method: AuthCodeReciceved; Session created. Expires in {0}", _session.ExpiresIn);
            }
            catch (Exception ex)
            {
                _logger.Trace(ex, "Module: Box; Method: AuthCodeReciceved; Error: error creating session.");
            }
            finally
            {
                if (_session != null)
                {
                    IsAuthenticated = true;

                    _sessionMaintainer = new Thread(MaintainSession);
                }
            }
        }

        private void MaintainSession()
        {
            if (!IsAuthenticated)
            {
                _logger.Warn("Box module: Account maintainer failed. Not authenticated.");
            }
            if (_session == null)
            {
                _logger.Warn("Box module: Account maintainer failed. Session null.");
            }

            if (_session != null)
            {
                Thread.Sleep(_session.ExpiresIn - 100);
                _session = _client.Auth.RefreshAccessTokenAsync(_session.RefreshToken).Result;
            }
            _logger.Warn("Box module: Account maintainer: session updated.");
        }

        #endregion Auth

        #region IModule

        public void Dispose()
        {
            _sessionMaintainer.Abort();
        }

        #endregion IModule

        public event PropertyChangedEventHandler PropertyChanged;
    }
}