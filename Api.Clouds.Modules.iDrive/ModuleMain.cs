﻿using Api.Clouds.ModuleBase;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Api.Clouds.Modules.iDrive
{
    [Export(typeof(IModule)), PartCreationPolicy(CreationPolicy.NonShared)]
    [ExportModule(ModuleName = "DropBox", AuthenticationType = AuthenticationType.Simple)]
    public class ModuleMain : IModule
    {
        private RestClient Client;
        private string ServerAddress;
        private AccountSimple accountInfo;
        public async Task UploadFileAsync(byte[] file, string fileName)
        {
            if (!IsAuthenticated) return;

            await Task.Run(() =>
            {
                Client.BaseUrl = "https://"+ServerAddress+"/evs/uploadFile";
                var request = new RestRequest();
                request.Method = Method.POST;
                request.Parameters.Add(new Parameter() { Name = "uid", Value = ((AccountSimple)accountInfo).Login, Type = ParameterType.GetOrPost });
                request.Parameters.Add(new Parameter() { Name = "pwd", Value = ((AccountSimple)accountInfo).Password, Type = ParameterType.GetOrPost });
                request.Parameters.Add(new Parameter() { Name = "p", Value = "/Upload", Type = ParameterType.GetOrPost });
                request.AddFile("file", file, fileName);
                //var param = new Parameter()
                //{
                //    Name = "file",
                //    Value = file,
                //    Type = ParameterType.RequestBody
                //};
                //request.AddParameter(param);

                var response = Client.Execute(request);


                var awe = "123";
            });
        }

        public async Task<byte[]> DownloadFileAsync(string fileName)
        {
            throw new NotImplementedException();
        }

        #region Authentication
        public async Task Authenticate(AccountInfo accountInfo)
        {
            await Task.Run(() =>
            {
                Client = new RestClient();
                Client.BaseUrl = "https://evs.idrive.com/evs/getServerAddress";
                RestRequest request = new RestRequest();
                request.Method = Method.POST;
                request.RequestFormat = DataFormat.Xml;
                request.Parameters.Add(new Parameter() { Name = "uid", Value = ((AccountSimple)accountInfo).Login, Type = ParameterType.GetOrPost });
                request.Parameters.Add(new Parameter() { Name = "pwd", Value = ((AccountSimple)accountInfo).Password, Type = ParameterType.GetOrPost });
                var response = Client.Execute(request);
                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(response.Content);
                var trees = xmlDoc.GetElementsByTagName("tree");
                if (trees[0].Attributes["message"].Value == "ERROR")
                    return;
                var addr = trees[0].Attributes["webApiServer"].Value;
                ServerAddress = addr;
                this.accountInfo = (AccountSimple)accountInfo;
                IsAuthenticated = true;
            });
        }

        public async Task<AccountInfo> ReturnAccessToken()
        {
            throw new NotImplementedException();
        }
        #endregion

        public bool IsAuthenticated { get; set; }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
