﻿using System;
using System.Threading.Tasks;
using Api.Clouds.ModuleBase;
using System.ComponentModel.Composition;
using Disk.SDK;
using Disk.SDK.Provider;
using Api.Clouds.Modules.YandexDisk;
using System.IO;
using System.Threading;
using NLog;
using System.Collections.Generic;
using System.ComponentModel;
using Api.Clouds.ModuleBase.Utils;

namespace Api.Clouds.Modules.YandexDisk
{
    [Export(typeof(IModule)), PartCreationPolicy(CreationPolicy.NonShared)]
    [ExportModule(ModuleName = "YandexDisk", AuthenticationType = AuthenticationType.OAuth)]
    class YandexDiskModule : IModule, INotifyPropertyChanged
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private DiskSdkClient _client;
        private string _accessToken = "";
        private bool _blocked;

        #region Properties
        private bool _isAuthenticated;
        public bool IsAuthenticated
        {
            get
            {
                return _isAuthenticated;
            }
            set
            {
                if (_isAuthenticated == value) return;
                _isAuthenticated = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsAuthenticated"));
            }
        }

        public string ModuleName => "YandexDisk";

        public string IconName => AppDomain.CurrentDomain.BaseDirectory + @"Resources\Images\yandexdisk-icon.png";
        #endregion

        #region IModule
        [ImportingConstructor]
        public YandexDiskModule()
        {
            _client = new DiskSdkClient();
        }
        public void Dispose()
        {
        }
        #endregion

        #region Sync
        public UploadProcess Upload(string localFileName, string cloudFileName)
        {
            if (_client == null)
            {
                _logger.Error("Module: YandexDisk; Method: Upload; Error: _client is null.");
                return null;
            }

            if (!IsAuthenticated)
            {
                _logger.Error("Module: YandexDisk; Method: Upload; Error: not authenticated.");
                return null;
            }
            if (!File.Exists(localFileName))
            {
                _logger.Error("Module: YandexDisk; Method: Upload; Error: file not exists.");
            }

            _client.UploadFileAsync();





            _logger.Info("YandexDisk module: Upload start. FileName: " + fileName);
            if (!IsAuthenticated) return;
            if (fileName == null)
                throw new ArgumentNullException("fileName");
            

            _client.UploadFileAsync(fileName, stream, new AsyncProgress((x, y) => { }), UnblockVoid);
            _blocked = true;
            while (_blocked)
            {
                Thread.Sleep(100);
            }
            _logger.Info("YandexDisk module: File uploaded. FileName: " + fileName);
            if (this.UploadProgressComplete != null)
            {
                this.UploadProgressComplete(this, new OperationResult() { Result = OperationResultEnum.OK, FileName = fileName });
            }
        }
        public void Download(Stream outStream, string fileName)
        {
            _logger.Info("YandexDisk module: Download start. FileName: " + fileName);
            //Пример:
            //https://github.com/yandex-disk/yandex-disk-sdk-csharp/blob/master/Samples/SdkSample.WPF/MainWindow.xaml.cs
            if (!IsAuthenticated) return;
            if (fileName == null)
                throw new ArgumentNullException("fileName");
            if (_client == null)
                throw new InvalidOperationException("YandexDisk module: Client is Null!");
            _client.DownloadFileAsync(fileName, outStream, new AsyncProgress((x, y) => { }), UnblockVoid);
            _blocked = true;
            while (_blocked)
            {
                Thread.Sleep(100);
            }
            _logger.Info("YandexDisk module: Download success.");
            if (this.DownloadProgressComplete != null)
            {
                this.DownloadProgressComplete(this, new OperationResult() { Result = OperationResultEnum.OK, FileName = fileName });
            }
        }
        public List<string> GetFiles()
        {
            if (!IsAuthenticated)
            {
                _logger.Warn("YandexDisk module: GetFiles failed. Not authenticated.");
                return null;
            }

            List<string> result = new List<string>();
            bool completed = false;
            _client.GetListAsync();
            _client.GetListCompleted += (sender, e) =>
            {
                if(e.Error != null)
                {
                    _logger.Warn("YandexDisk module: GetFiles method. Error getting files.", e.Error);
                    result = null;
                    completed = true;
                }
                var files = (List<DiskItemInfo>)e.Result;
                files.ForEach(x=>result.Add(x.DisplayName));

                completed = true;
            };

            while (!completed)
            {
                Thread.Sleep(100);
            }
            return result;
        }

        private void UnblockVoid(object sender, SdkEventArgs e)
        {
            _blocked = false;
        }

        #region NotImplementedFuncs
        public void ProgressChanged(ulong current, ulong total) //Для прогрессбара
        {

        }
        public void UploadComplete(object sender, SdkEventArgs e) //Вызывается по окончании загрузки
        {
        }
        #endregion

        #endregion

        #region Async
        public async Task UploadFileAsync(Stream stream, string fileName)
        {
            await Task.Run(() => { Upload(stream, fileName); });
        }
        public async Task DownloadFileAsync(Stream outStream, string fileName)
        {
            await Task.Run(() => { Download(outStream, fileName); });
        }
        #endregion

        #region Authentication
        public async Task Authenticate(AccountInfo accountInfo)
        {
            Authenticate();
        }
        public async Task Authenticate()
        {
            _logger.Info("YandexDisk module: Auth started.");
            LoginScreen ls = new LoginScreen(_client);
            ls.AuthCompleted += ls_AuthCompleted;
            ls.Show();
            _logger.Info("YandexDisk module: Login screen activated.");
        }
        void ls_AuthCompleted(object sender, GenericSdkEventArgs<string> e)
        {
            _logger.Info("YandexDisk module: Login screen closed.");

            if (e.Error == null)
            {
                _accessToken = e.Result;
                _client = new DiskSdkClient(_accessToken);
                IsAuthenticated = true;
                _logger.Info("YandexDisk module: Auth succsess.");
                if (this.AuthComplete != null)
                {
                    this.AuthComplete(this, new AccountYandexDisk() { Token = _accessToken });
                }
            }
            else
            {
                _logger.Error("YandexDisk module: Auth failed.", e.Error);
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Событие возникает при успешном завершении авторизации
        /// </summary>
        public EventHandler<AccountInfo> AuthComplete { get; set; }
        /// <summary>
        /// Not implemented
        /// </summary>
        public EventHandler<OperationProgressChangedEventArgs> UploadProgressChanged { get; set; }
        /// <summary>
        /// Not implemented
        /// </summary>
        public EventHandler<OperationResult> UploadProgressComplete { get; set; }
        /// <summary>
        /// Not implemented
        /// </summary>
        public EventHandler<OperationProgressChangedEventArgs> DownloadProgressChanged { get; set; }
        /// <summary>
        /// Not implemented
        /// </summary>
        public EventHandler<OperationResult> DownloadProgressComplete { get; set; }
        #endregion


        public event PropertyChangedEventHandler PropertyChanged;
    }
}
