﻿using Disk.SDK;
using Disk.SDK.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Api.Clouds.Modules.YandexDisk
{
    /// <summary>
    /// Interaction logic for LoginScreen.xaml
    /// </summary>
    public partial class LoginScreen : Window
    {
        public LoginScreen()
        {
            InitializeComponent();
        }

        public LoginScreen(IDiskSdkClient Client) : this()
        {
            Client.AuthorizeAsync(new WebBrowserWrapper(this.browser), "9153338374284fc781e3ef491445a6ad", "https://oauth.yandex.ru/verification_code?ncrnd=3939", AuthCallBack);
        }

        private void AuthCallBack(object sender, GenericSdkEventArgs<string> e)
        {
            if (this.AuthCompleted != null)
            {
                this.AuthCompleted(this, new GenericSdkEventArgs<string>(e.Result));
            }

            this.Close();
        }

        public event EventHandler<GenericSdkEventArgs<string>> AuthCompleted;
    }
}
