﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Clouds.Sandbox
{
    internal class Sandbox
    {
        private readonly ModulesManager _modulesManager;

        public Sandbox()
        {
            // Чтобы загрузить разделенные файлы в облака
            // создаем менеджер модулей
            _modulesManager = new ModulesManager();
        }

        public async Task Authenticate()
        {
            // Выполняем авторизацию во всех облаках (функция AuthAll в ModulesManager)
            await _modulesManager.AuthAll();
        }

        public async Task SplitAndUpload()
        {
            // Тут производятся действия по разделению секрета
            // и формируются файлы для загрузки
            // например test1.bin и test2.bin

            // Загрузим test1.bin в DropBox и test2.bin в Mega
            // для этого вызовем метод модуля Upload
            // этот метод может быть вызван двумя способами
            // 1) UploadFileAsync("имя загружаемого файла в локальной ФС", "имя с которым загруженный файл будет сохранен в облаке")
            // 1) UploadFileAsync("Stream (поток, с открытым файлом в режиме чтения)", "имя с которым загруженный файл будет сохранен в облаке")
            var test1UploadProcess = await _modulesManager.DropBox.UploadFileAsync("test1.bin", "test1.bin");
            var test2UploadProcess = await _modulesManager.Mega.UploadFileAsync("test2.bin", "test2.bin");
            //var test3UploadProcess = await _modulesManager.Box.UploadFileAsync("test2.bin", "test2.bin");
            //var test4UploadProcess = await _modulesManager.GoogleDrive.UploadFileAsync("test2.bin", "test2.bin");
            //var test5UploadProcess = await _modulesManager.OneDrive.UploadFileAsync("test2.bin", "test2.bin");

            // Метод Upload всех модулей возвращает экземпляр класса LoadProcess
            // этот класс содержит поля:
            // FileInfo FileInfo - информация о локальном загруженном файле (стандартный класс C#, можно получить имя и размер)
            // TimeSpan ElapsedTime - информация о затраченном времени на загрузку (стандартный класс C#, временной промежуток)
            // double GetSpeed() - метод, вычисляющий среднюю скорость загрузки по формуле (размер файла)/(затраченное время)
        }
    }
}