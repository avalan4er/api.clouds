﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Api.Clouds.Sandbox
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly Sandbox _sandbox;

        public MainWindow()
        {
            InitializeComponent();

            _sandbox = new Sandbox();
        }

        private async void HandleAuthButtonClick(object sender, RoutedEventArgs e)
        {
            await _sandbox.Authenticate();
        }

        private async void HandleUploadButtonClick(object sender, RoutedEventArgs e)
        {
            await _sandbox.SplitAndUpload();
        }
    }
}