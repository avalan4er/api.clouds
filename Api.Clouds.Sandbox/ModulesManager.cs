﻿using Api.Clouds.ModuleBase;
using Api.Clouds.Modules.Box;
using Api.Clouds.Modules.DropBox;
using Api.Clouds.Modules.GoogleDrive;
using Api.Clouds.Modules.Mega;
using Api.Clouds.Modules.OneDrive;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;

namespace Api.Clouds.Sandbox
{
    internal class ModulesManager
    {
        public ModulesManager()
        {
            Box = new BoxModule();
            DropBox = new DropBoxModule();
            GoogleDrive = new GoogleDriveModule();
            Mega = new MegaModule();
            OneDrive = new OneDriveModule();
        }

        #region Properties

        public IModule Box { get; private set; }
        public IModule DropBox { get; private set; }
        public IModule GoogleDrive { get; private set; }
        public IModule Mega { get; private set; }
        public IModule OneDrive { get; private set; }

        #endregion Properties

        #region Commands

        public async Task AuthAll()
        {
            foreach (var module in GetAllModules())
            {
                await module.Authenticate();
            }
        }

        public List<IModule> GetAllModules()
        {
            return new List<IModule>()
            {
                Box,
                DropBox,
                GoogleDrive,
                Mega,
                OneDrive
            };
        }

        #endregion Commands
    }
}