﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Api.Clouds.Modules.DropBox
{
    /// <summary>
    /// Interaction logic for LoginScreen.xaml
    /// </summary>
    public partial class LoginScreen : Window
    {
        private string Uri;

        public LoginScreen()
        {
            InitializeComponent();
        }

        public LoginScreen(string uri) : this()
        {
            this.Uri = uri;
            Browser.Loaded += Browser_Loaded;
        }

        void Browser_Loaded(object sender, RoutedEventArgs e)
        {
            Browser.Navigate(new Uri(Uri));
            Browser.Navigated += Browser_Navigated;
        }

        void Browser_Navigated(object sender, NavigationEventArgs e)
        {
            //OriginalString = "https://www.dropbox.com/1/oauth/authorize_submit"

            if(e.Uri.OriginalString.Equals("https://www.dropbox.com/1/oauth/authorize_submit"))
            {
                if (this.AuthCompleted != null)
                {
                    this.AuthCompleted(this, "OK");
                }
                this.Close();
            }
        }

        public event EventHandler<string> AuthCompleted;

    }
}
