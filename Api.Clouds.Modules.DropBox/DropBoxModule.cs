﻿using Api.Clouds.ModuleBase;
using Api.Clouds.ModuleBase.Utils;
using DropNetRT;
using DropNetRT.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using AccountInfo = Api.Clouds.ModuleBase.AccountInfo;

namespace Api.Clouds.Modules.DropBox
{
    [Export(typeof(IModule)), PartCreationPolicy(CreationPolicy.NonShared)]
    [ExportModule(ModuleName = "DropBox", AuthenticationType = AuthenticationType.OAuth)]
    public class DropBoxModule : IModule, INotifyPropertyChanged
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private DropNetClient _client;

        #region Properties

        private bool _isAuthenticated;
        public string IconName => AppDomain.CurrentDomain.BaseDirectory + @"Resources\Images\dropbox-icon.png";

        public bool IsAuthenticated
        {
            get
            {
                return _isAuthenticated;
            }
            set
            {
                if (_isAuthenticated == value) return;
                _isAuthenticated = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsAuthenticated"));
            }
        }

        public string ModuleName => "DropBox";

        #endregion Properties

        #region IModule

        [ImportingConstructor]
        public DropBoxModule()
        {
        }

        public void Dispose()
        {
            _client = null;
        }

        #endregion IModule

        #region Sync

        public LoadProcess Download(Stream stream, string cloudFileName)
        {
            if (!IsAuthenticated)
            {
                _logger.Error("Module: DropBox; Method: Download; File name: {0}; Error: Not authenicated.", cloudFileName);
                return null;
            }

            var downloadProcess = new LoadProcess();

            try
            {
                var stopwatch = Stopwatch.StartNew();
                var bytes = _client.GetFile("/" + cloudFileName).Result;
                stopwatch.Stop();
                downloadProcess.ElapsedTime = stopwatch.Elapsed;
                stream.Write(bytes, 0, bytes.Length);
            }
            catch (Exception)
            {
                _logger.Error("Module: DropBox; Method: Download; File name: {0}; Error: Download failed.", cloudFileName);
                return null;
            }

            downloadProcess.FileInfo = new FileInfo((stream as FileStream)?.Name);
            _logger.Trace("Module: DropBox; Method: Download; File name: {0}; File downloaded");

            return downloadProcess;
        }

        public LoadProcess Download(string localFileName, string cloudFileName)
        {
            if (!IsAuthenticated)
            {
                _logger.Error("Module: DropBox; Method: Download; File name: {0}; Error: Not authenicated.", cloudFileName);
                return null;
            }

            var downloadProcess = new LoadProcess();

            try
            {
                var stopwatch = Stopwatch.StartNew();
                var bytes = _client.GetFile("/" + cloudFileName).Result;
                stopwatch.Stop();
                downloadProcess.ElapsedTime = stopwatch.Elapsed;

                using (var fileStream = new FileStream(localFileName, FileMode.Create, FileAccess.Write))
                {
                    fileStream.Write(bytes, 0, bytes.Length);
                }
            }
            catch (Exception)
            {
                _logger.Error("Module: DropBox; Method: Download; File name: {0}; Error: Download failed.", cloudFileName);
                return null;
            }

            downloadProcess.FileInfo = new FileInfo(localFileName);
            _logger.Trace("Module: DropBox; Method: Download; File name: {0}; File downloaded");

            return downloadProcess;
        }

        public List<string> GetFiles()
        {
            if (!IsAuthenticated)
            {
                _logger.Error("Module: DropBox; Method: GetFiles; Error: Not authenticated.");
                return null;
            }
            List<Metadata> metaDataContents;

            try
            {
                metaDataContents = _client.GetMetaData("/", null)?.Result?.Contents;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Module: DropBox; Method: GetFiles; Error: Error getting files list.");
                return null;
            }

            List<string> result = new List<string>();
            metaDataContents.ForEach(x => result.Add(x.Name));

            return result;
        }

        public LoadProcess Upload(Stream stream, string cloudFileName)
        {
            if (!IsAuthenticated)
            {
                _logger.Error("Module: DropBox; Method: Uplaod; Error: Not authenticated.");
                return null;
            }

            var uploadProcess = new LoadProcess()
            {
                FileInfo = new FileInfo((stream as FileStream)?.Name),
            };

            try
            {
                var metaData = _client.GetMetaData(cloudFileName, null).Result;

                if (metaData != null)
                {
                    _logger.Trace(
                        "Module: DropBox; Method: Upload; File name: {0}; File already exists in cloud. Deleting...",
                        cloudFileName);

                    var result = _client.Delete(cloudFileName).Result;
                    if (result.IsDeleted)
                    {
                        _logger.Trace("Module: DropBox; Method: Upload;  File name: {0}; File deleted.", cloudFileName);
                    }
                    else
                    {
                        _logger.Warn("Module: DropBox; Method: Upload; File name: {0}; Error: Delete file failed.");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Trace(ex, "Module: DropBox; Method: Upload; File name {0}; Error: Failed to get metadata - file not exists;", cloudFileName);
            }

            using (var memoryStram = new MemoryStream())
            {
                stream.CopyTo(memoryStram);

                try
                {
                    var stopWatch = Stopwatch.StartNew();
                    _client.Upload("/", cloudFileName, memoryStram.ToArray());
                    stopWatch.Stop();
                    uploadProcess.ElapsedTime = stopWatch.Elapsed;

                    _logger.Trace("Module: DropBox; Method: Upload; File name: {0}; File uploaded.", cloudFileName);
                }
                catch (Exception)
                {
                    _logger.Error("Module: DropBox; Method: Upload; File name {0}; Error: Upload failed.", cloudFileName);
                    return null;
                }
            }

            return uploadProcess;
        }

        public LoadProcess Upload(string localFileName, string cloudFileName)
        {
            if (!IsAuthenticated)
            {
                _logger.Error("Module: DropBox; Method: Uplaod; Error: Not authenticated.");
                return null;
            }
            if (!File.Exists(localFileName))
            {
                _logger.Error("Module: DropBox; Method: Upload; File name: {0}; Error: File not exists.", localFileName);
                return null;
            }

            var uploadProcess = new LoadProcess()
            {
                FileInfo = new FileInfo(localFileName),
            };

            try
            {
                var metaData = _client.GetMetaData(cloudFileName, null);

                if (metaData != null)
                {
                    _logger.Trace(
                        "Module: DropBox; Method: Upload; File name: {0}; File already exists in cloud. Deleting...",
                        cloudFileName);

                    var result = _client.Delete(cloudFileName).Result;
                    if (result.IsDeleted)
                    {
                        _logger.Trace("Module: DropBox; Method: Upload;  File name: {0}; File deleted.", cloudFileName);
                    }
                    else
                    {
                        _logger.Warn("Module: DropBox; Method: Upload; File name: {0}; Error: Delete file failed.");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Trace(ex, "Module: DropBox; Method: Upload; File name {0}; Error: Failed to get metadata - file not exists;", cloudFileName);
            }

            using (var fileStream = new FileStream(localFileName, FileMode.Open, FileAccess.Read))
            {
                using (var memoryStram = new MemoryStream())
                {
                    fileStream.CopyTo(memoryStram);

                    try
                    {
                        var stopWatch = Stopwatch.StartNew();
                        _client.Upload("/", cloudFileName, memoryStram.ToArray());
                        stopWatch.Stop();
                        uploadProcess.ElapsedTime = stopWatch.Elapsed;

                        _logger.Trace("Module: DropBox; Method: Upload; File name: {0}; File uploaded.", cloudFileName);
                    }
                    catch (Exception)
                    {
                        _logger.Error("Module: DropBox; Method: Upload; File name {0}; Error: Upload failed.", cloudFileName);
                        return null;
                    }
                }
            }

            return uploadProcess;
        }

        #endregion Sync

        #region Async

        public async Task<LoadProcess> DownloadFileAsync(Stream stream, string cloudFileName)
        {
            return await Task.Run(() => Download(stream, cloudFileName));
        }

        public async Task<LoadProcess> DownloadFileAsync(string localFileName, string cloudFileName)
        {
            return await Task.Run(() => Download(localFileName, cloudFileName));
        }

        public async Task<LoadProcess> UploadFileAsync(Stream stream, string cloudFileName)
        {
            return await Task.Run(() => Upload(stream, cloudFileName));
        }

        public async Task<LoadProcess> UploadFileAsync(string localFileName, string cloudFileName)
        {
            return await Task.Run(() => Upload(localFileName, cloudFileName));
        }

        #endregion Async

        #region Authentication

        public async Task Authenticate(AccountInfo accountInfo)
        {
            await Task.Run(() =>
            {
                var accountDropBox = accountInfo as AccountDropBox;
                if (accountDropBox != null)
                    _client = new DropNetClient("yjujusp8utymm3v", "bc5bkzaeo5sya0t")
                    {
                        UseSandbox = true,
                        UserLogin =
                            new UserLogin
                            {
                                Token = accountDropBox.Token,
                                Secret = accountDropBox.Secret
                            }
                    };
                IsAuthenticated = true;
            });
        }

        public async Task Authenticate()
        {
            await Task.Run(() =>
            {
                _client = new DropNetClient("yjujusp8utymm3v", "bc5bkzaeo5sya0t") { UseSandbox = true };
                var url = _client.BuildAuthorizeUrl(_client.GetRequestToken().Result);

                Application.Current.Dispatcher.Invoke(() =>
                {
                    var ls = new LoginScreen(url);
                    ls.Show();
                    ls.AuthCompleted += ls_AuthCompleted;
                });
            });
        }

        private void ls_AuthCompleted(object sender, string e)
        {
            _client.GetAccessToken();
            IsAuthenticated = true;
        }

        #endregion Authentication

        public event PropertyChangedEventHandler PropertyChanged;
    }
}