﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Api.Clouds.ModuleBase.Utils;

namespace Api.Clouds.ModuleBase
{
    public interface IModule : IDisposable
    {

        #region Properties
        bool IsAuthenticated { get; set; }
        string ModuleName { get; }
        string IconName { get; }
        #endregion

        #region Authentication
        Task Authenticate(AccountInfo accountInfo);
        Task Authenticate();
        #endregion

        #region Async
        Task<LoadProcess> UploadFileAsync(string localFileName, string cloudFileName);
        Task<LoadProcess> DownloadFileAsync(string localFileName, string cloudFileName);

        Task<LoadProcess> UploadFileAsync(Stream stream, string cloudFileName);
        Task<LoadProcess> DownloadFileAsync(Stream stream, string cloudFileName);
        #endregion

        #region Sync Methods
        LoadProcess Upload(string localFileName, string cloudFileName);
        LoadProcess Download(string localFileName, string cloudFileName);

        LoadProcess Upload(Stream stream, string cloudFileName);
        LoadProcess Download(Stream stream, string cloudFileName);
        List<string> GetFiles();
        #endregion
    }

}
