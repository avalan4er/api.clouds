﻿using Api.Clouds.ModuleBase.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Clouds.ModuleBase.Messages
{
    public class DownloadComplete
    {
        public LoadProcess DownloadProcess { get; set; }
    }
}
