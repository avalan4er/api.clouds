﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Clouds.ModuleBase.Utils
{
    public class LoadProcess
    {
        public Guid Id { get; } = Guid.NewGuid();

        public FileInfo FileInfo { get; set; }

        public TimeSpan ElapsedTime { get; set; }

        public double GetSpeed()
        {
            var fileSize = this.FileInfo.Length / 1024;
            var elapsedInSeconds = this.ElapsedTime.TotalSeconds;

            return fileSize / elapsedInSeconds;
        }
    }
}
