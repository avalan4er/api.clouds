﻿using Api.Clouds.ModuleBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.ComponentModel.Composition.ReflectionModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Clouds.ModuleBase
{
    public class ModuleInstanceFactory
    {
        public static IModule CreateInstance(string contractName)
        {
            return CompositionContainerSingleton.Instance.GetExportedValues<IModule>().SingleOrDefault(x => x.GetType().FullName == contractName);
        }

        public static IEnumerable<Type> GetExportedTypes()
        {
            return CompositionContainerSingleton.Instance.Catalog.Parts
                .Select(part => ComposablePartExportType<IModule>(part))
                .Where(t => t != null).ToList();
        }

        private static Type ComposablePartExportType<T>(ComposablePartDefinition part)
        {

            if (part.ExportDefinitions.Any(
                def => def.Metadata.ContainsKey("ExportTypeIdentity") &&
                    def.Metadata["ExportTypeIdentity"].Equals(typeof(T).FullName)))
            {
                return ReflectionModelServices.GetPartType(part).Value;
            }
            return null;
        }
    }
}
