﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Clouds.ModuleBase
{
    public class OperationResult
    {
        public OperationResultEnum Result { get; set; }
        public string FileName { get; set; }
    }

    public enum OperationResultEnum
    {
        OK,
        ERROR,
    }

    public struct OperationProgressChangedEventArgs
    {
        public double Value { get; set; }
    }
}
