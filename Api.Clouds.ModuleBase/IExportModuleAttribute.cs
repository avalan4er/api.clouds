﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Clouds.ModuleBase
{
    public enum AuthenticationType
    {
        Simple,
        OAuth
    }

    public interface IExportModuleAttribute
    {
        string ModuleName { get; }
        AuthenticationType AuthenticationType { get; }
    }
}
