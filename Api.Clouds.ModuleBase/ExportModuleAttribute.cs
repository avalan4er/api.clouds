﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;

namespace Api.Clouds.ModuleBase
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class ExportModuleAttribute : ExportAttribute, IExportModuleAttribute
    {
        public string ModuleName { get; set; }
        public AuthenticationType AuthenticationType { get; set; }
    }
}
