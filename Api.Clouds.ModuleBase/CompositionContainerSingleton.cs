﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.ReflectionModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Clouds.ModuleBase
{
    public class CompositionContainerSingleton
    {
        [ImportMany(typeof(IModule))]
        public static readonly CompositionContainer Instance = GetContainer();

        CompositionContainerSingleton() { }

        private static CompositionContainer GetContainer()
        {
            string pluginsPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Modules");
            var modulesDirs = Directory.EnumerateDirectories(pluginsPath);
            AggregateCatalog catalog = new AggregateCatalog();
            foreach(var directory in modulesDirs)
            {
                DirectoryCatalog directoryCatalog = new DirectoryCatalog(Path.Combine(pluginsPath, directory), "Api.Clouds.*.dll");
                catalog.Catalogs.Add(directoryCatalog);
            }
            AssemblySource.Instance.AddRange(catalog.Parts.Select(part => ReflectionModelServices.GetPartType(part).Value.Assembly));
            var container = new CompositionContainer(catalog);
            return container;
        }
    }
}
