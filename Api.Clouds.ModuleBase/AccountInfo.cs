﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Api.Clouds.ModuleBase
{
    public class AccountInfo
    {
        private Guid _id = Guid.NewGuid();
        public Guid Id
        {
            get
            {
                return _id;
            }
        }
        public string ContractName { get; set; }
    }

    public class AccountBox : AccountInfo
    {
        public AccountBox()
        {
            ContractName = "Api.Clouds.Modules.Box.ModuleMain";
        }
        public int LifeTime { get; set; }
    }

    public class AccountDropBox : AccountInfo
    {
        public AccountDropBox()
        {
            ContractName = "Api.Clouds.Modules.DropBox.ModuleMain";
        }

        public string Token { get; set; }
        public string Secret { get; set; }
    }

    public class AccountMega : AccountInfo
    {
        public AccountMega()
        {
            ContractName = "Api.Clouds.Modules.Mega.ModuleMain";
        }
        public string Login;
        public string Password;
    }

    public class AccountSkyDrive : AccountInfo
    {
        public AccountSkyDrive()
        {
            ContractName = "Api.Clouds.Modules.SkyDrive.ModuleMain";
        }
    }

    public class AccountYandexDisk : AccountInfo
    {
        public AccountYandexDisk()
        {
            ContractName = "Api.Clouds.Modules.YandexDisk.ModuleMain";
        }
        public string Token { get; set; }
    }

    public class AccountGoogleDrive : AccountInfo
    {
        public AccountGoogleDrive()
        {
            ContractName = "Api.Clouds.Modules.GoogleDrive.ModuleMain";
        }
    }
}
