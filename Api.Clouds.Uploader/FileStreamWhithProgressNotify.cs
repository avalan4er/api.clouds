﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Win32.SafeHandles;
using System.ComponentModel;

namespace Api.Clouds.Uploader
{
    public class FileStreamWhithProgressNotify : FileStream, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private double _progress;
        public double Progress
        {
            get
            {
                return _progress;
            }
            set
            {
                if(_progress != value)
                {
                    _progress = value;
                    OnPropertyChanged("Progress");
                }
            }
        }

        public FileStreamWhithProgressNotify(string path, FileMode mode, FileAccess access) : base(path, mode, access)
        {
        }

        #region Overrides
        public override int Read(byte[] array, int offset, int count)
        {
            Progress = Position / (double)Length * 100;
            return base.Read(array, offset, count);
        }

        public override void Write(byte[] array, int offset, int count)
        {
            Progress = Position / (double)Length * 100;
            base.Write(array, offset, count);
        } 
        #endregion

        #region Utils
        private void OnPropertyChanged(string propertyName)
        {
            if(PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
