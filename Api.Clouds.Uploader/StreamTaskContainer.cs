﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Api.Clouds.Uploader
{
    public class StreamTaskContainer
    {

        public ObservableCollection<StreamTask> Tasks { get; } = new ObservableCollection<StreamTask>();

        public StreamTaskContainer()
        {

        }

        public void Add(StreamTask task)
        {
            Tasks.Add(task);
        }

        public async Task WaitAll(CancellationToken ct)
        {
            await Task.Run(() =>
            {
                while (Tasks.Where(x => x.State != StreamTaskState.Finished).Any())
                {
                    ct.ThrowIfCancellationRequested();
                    Thread.Sleep(10);
                }
            });
        }
    }
}
