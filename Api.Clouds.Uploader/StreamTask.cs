﻿using Api.Clouds.ModuleBase;
using Api.Clouds.ModuleBase.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Clouds.Uploader
{

    public enum StreamTaskState
    {
        NotStarted,
        InProgress,
        Finished
    }

    public class StreamTask : INotifyPropertyChanged
    {
        private readonly string _filePath;
        private readonly IModule _loader;
        private readonly int _attemptsMaxCount = 5;
        public event PropertyChangedEventHandler PropertyChanged;

        #region Properties

        private double _progress;
        public double Progress
        {
            get
            {
                return _progress;
            }
            set
            {
                if(_progress != value)
                {
                    _progress = value;
                    OnPropertyChanged("Progress");
                }
            }
        }

        private StreamTaskState _state;
        public StreamTaskState State
        {
            get
            {
                return _state;
            }
            set
            {
                if (_state != value)
                {
                    _state = value;
                    OnPropertyChanged("State");
                }
            }
        }

        private string _fileName;
        public string FileName
        {
            get
            {
                return _fileName;
            }
            set
            {
                if(_fileName != value)
                {
                    _fileName = value;
                    OnPropertyChanged("FileName");
                }
            }
        }

        private LoadProcess _loadProcess;
        public LoadProcess LoadProcess
        {
            get
            {
                return _loadProcess;
            }
            set
            {
                if(_loadProcess != value)
                {
                    _loadProcess = value;
                    OnPropertyChanged("LoadProcess");
                }
            }
        }

        public IModule Loader
        {
            get
            {
                return _loader;
            }
        }

        #endregion

        public StreamTask(string filePath, IModule loader)
        {
            _filePath = filePath;
            _loader = loader;
            State = StreamTaskState.NotStarted;
            FileName = Path.GetFileName(filePath);
        }

        #region Commands

        public async Task Upload()
        {
            int uploadAttempts = 0;
            using (var stream = new FileStreamWhithProgressNotify(_filePath, FileMode.Open, FileAccess.Read))
            {
                stream.PropertyChanged += Stream_PropertyChanged;
                State = StreamTaskState.InProgress;

                while (uploadAttempts < _attemptsMaxCount)
                {
                    LoadProcess = await _loader.UploadFileAsync(stream, FileName);
                    if(LoadProcess != null)
                    {
                        break;
                    }
                    uploadAttempts++;
                }


                State = StreamTaskState.Finished;
                stream.PropertyChanged -= Stream_PropertyChanged;
            }
        }

        public async Task Download()
        {
            int downloadAttempts = 0;
            using (var stream = new FileStreamWhithProgressNotify(Path.Combine("Temp", _filePath), FileMode.Create, FileAccess.Write))
            {
                stream.PropertyChanged += Stream_PropertyChanged;
                State = StreamTaskState.InProgress;

                while (downloadAttempts < _attemptsMaxCount)
                {
                    LoadProcess = await _loader.DownloadFileAsync(stream, FileName);
                    if(LoadProcess != null)
                    {
                        break;
                    }
                    downloadAttempts++;
                }

                State = StreamTaskState.Finished;
                stream.PropertyChanged -= Stream_PropertyChanged;
            }
        }

        #endregion

        #region Utils

        private void Stream_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "Progress") return;

            var stream = sender as FileStreamWhithProgressNotify;
            if (stream == null) return;

            Progress = stream.Progress;
        }

        private void OnPropertyChanged(string propertyName)
        {
            if(PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
