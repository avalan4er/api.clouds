﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Live;
using System.Web;
using NLog;

namespace Api.Clouds.Modules.OneDrive
{
    /// <summary>
    /// Логика взаимодействия для LoginScreen.xaml
    /// </summary>
    public partial class LoginScreen : Window
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        string Url;
        LiveAuthClient liveAuthClient;
        public LoginScreen()
        {
            InitializeComponent();
        }

        public LoginScreen(LiveAuthClient authClient)
            : this()
        {
            this.Url = authClient.GetLoginUrl(new string[] { "wl.basic", "wl.skydrive", "wl.skydrive_update"});
            this.liveAuthClient = authClient;
            Browser.Loaded += Browser_Loaded;
        }

        void Browser_Loaded(object sender, RoutedEventArgs e)
        {
            Browser.Navigate(Url);
            Browser.Navigated += Browser_Navigated;
        }

        void Browser_Navigated(object sender, NavigationEventArgs e)
        {
            //https://login.live.com/oauth20_desktop.srf?code=b4f0f0de-6a5f-b26e-ede7-b98ce07f6507&lc=1049
            if(e.Uri.AbsoluteUri.Contains("https://login.live.com/oauth20_desktop.srf?code"))
            {
                _logger.Info("SkyDrive module: Browser auth complete.");
                Dictionary<string, string> responseParams = e.Uri.AbsoluteUri.Split('?')[1].Split('&').Select(_ => _.Split('=')).Select(_ => new { Key = _[0], Vale = _[1] }).ToDictionary(_ => _.Key, _ => _.Vale);
                _logger.Info("SkyDrive module: Browser code got.");
                var session = liveAuthClient.ExchangeAuthCodeAsync(responseParams.SingleOrDefault(x => x.Key == "code").Value).Result;
                _logger.Info("SlyDrive module: Browser session created.");
                if(AuthComplete != null && session != null)
                {
                    AuthComplete(this, session);
                }
                this.Close();
            }
        }

        public EventHandler<LiveConnectSession> AuthComplete { get; set; }
    }
}
