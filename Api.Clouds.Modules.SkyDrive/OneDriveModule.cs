﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Api.Clouds.ModuleBase;
using Api.Clouds.ModuleBase.Utils;
using Microsoft.Live;
using NLog;

namespace Api.Clouds.Modules.OneDrive
{
    [Export(typeof(IModule)), PartCreationPolicy(CreationPolicy.NonShared)]
    [ExportModule(ModuleName = "OneDrive", AuthenticationType = AuthenticationType.OAuth)]
    public class OneDriveModule : IModule, INotifyPropertyChanged
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private LiveConnectClient _client;

        #region IModule
        [ImportingConstructor]
        public OneDriveModule()
        {
        }

        public void Dispose()
        {
        }
        #endregion

        #region Properties
        private bool _isAuthenticated;
        public bool IsAuthenticated
        {
            get
            {
                return _isAuthenticated;
            }
            set
            {
                if (_isAuthenticated == value) return;
                _isAuthenticated = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsAuthenticated"));
            }
        }

        public string ModuleName => "OneDrive";

        public string IconName => AppDomain.CurrentDomain.BaseDirectory + @"Resources\Images\skydrive-icon.png";
        #endregion

        #region Sync

        public LoadProcess Upload(Stream stream, string cloudFileName)
        {
            if (!IsAuthenticated)
            {
                _logger.Error("Module: OneDrive; Method: Upload; Error: not authenicated.");
                return null;
            }

            var uploadProcess = new LoadProcess()
            {
                FileInfo = new FileInfo((stream as FileStream)?.Name),
            };

            var stopwatch = Stopwatch.StartNew();
            var result =
                _client.UploadAsync("me/skydrive", cloudFileName, stream, OverwriteOption.Overwrite).Result;
            stopwatch.Stop();

            if (result == null)
            {
                _logger.Error("Module: OneDrive; Method: Upload; Error: upload failed.");
                return null;
            }

            uploadProcess.ElapsedTime = stopwatch.Elapsed;

            return uploadProcess;
        }

        public LoadProcess Upload(string localFileName, string cloudFileName)
        {
            if (!IsAuthenticated)
            {
                _logger.Error("Module: OneDrive; Method: Upload; Error: not authenicated.");
                return null;
            }
            if (!File.Exists(localFileName))
            {
                _logger.Error("Module: OneDrive; Method: Upload; Error: file not exists.");
                return null;
            }

            var uploadProcess = new LoadProcess()
            {
                FileInfo = new FileInfo(localFileName),
            };

            using (var fileStream = new FileStream(localFileName, FileMode.Open, FileAccess.Read))
            {
                var stopwatch = Stopwatch.StartNew();
                var result =
                    _client.UploadAsync("me/skydrive", cloudFileName, fileStream, OverwriteOption.Overwrite).Result;
                stopwatch.Stop();

                if (result == null)
                {
                    _logger.Error("Module: OneDrive; Method: Upload; Error: upload failed.");
                    return null;
                }

                uploadProcess.ElapsedTime = stopwatch.Elapsed;
            }

            return uploadProcess;
        }

        public LoadProcess Download(Stream stream, string cloudFileName)
        {
            if (!IsAuthenticated)
            {
                _logger.Error("Module: OneDrive; Method: Download; Error: not authenticated.");
                return null;
            }

            dynamic folderId = _client.GetAsync("me/skydrive/files").Result.Result;
            if (folderId == null)
            {
                _logger.Error("Module: OneDrive; Method: Download; Error: folder not found.");
                return null;
            }

            var downloadProcess = new LoadProcess();

            foreach (IDictionary<string, object> file in folderId.data)
            {
                if (file.Keys.Count == 0) continue;
                if (file.Values.ElementAt(12).ToString() == "folder") continue;
                if (string.Compare(file.Values.ElementAt(2).ToString(), cloudFileName, StringComparison.OrdinalIgnoreCase) == 0)
                {
                    var fileId = file.Values.ElementAt(0);

                    var stopwatch = Stopwatch.StartNew();
                    var result = _client.DownloadAsync(fileId + "/content").Result;
                    stopwatch.Stop();
                    downloadProcess.ElapsedTime = stopwatch.Elapsed;

                    result.Stream.CopyTo(stream);
                    stream.Flush();
                    break;
                }
            }

            downloadProcess.FileInfo = new FileInfo((stream as FileStream)?.Name);

            return downloadProcess;
        }

        public LoadProcess Download(string localFileName, string cloudFileName)
        {
            if (!IsAuthenticated)
            {
                _logger.Error("Module: OneDrive; Method: Download; Error: not authenticated.");
                return null;
            }

            dynamic folderId = _client.GetAsync("me/skydrive/files").Result.Result;
            if (folderId == null)
            {
                _logger.Error("Module: OneDrive; Method: Download; Error: folder not found.");
                return null;
            }

            var downloadProcess = new LoadProcess();

            foreach (IDictionary<string, object> file in folderId.data)
            {
                if (file.Keys.Count == 0) continue;
                if (file.Values.ElementAt(12).ToString() == "folder") continue;
                if (string.Compare(file.Values.ElementAt(2).ToString(), cloudFileName, StringComparison.OrdinalIgnoreCase) == 0)
                {
                    var fileId = file.Values.ElementAt(0);

                    var stopwatch = Stopwatch.StartNew();
                    var result = _client.DownloadAsync(fileId + "/content").Result;
                    stopwatch.Stop();
                    downloadProcess.ElapsedTime = stopwatch.Elapsed;

                    using (var fileStream = new FileStream(localFileName, FileMode.Create, FileAccess.Write))
                    {
                        result.Stream.CopyTo(fileStream);
                        fileStream.Flush();
                    }
                    break;
                }
            }

            downloadProcess.FileInfo = new FileInfo(localFileName);

            return downloadProcess;
        }

        public List<string> GetFiles()
        {
            if (!IsAuthenticated)
            {
                _logger.Warn("SkyDrive module: GetFiles failed. Not authenticated.");
                return null;
            }
            IDictionary<string, object> files;

            try
            {
                files = _client.GetAsync("me/skydrive/files").Result.Result;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "SkyDrive module: GetFiles method. Error getting files.");
                return null;
            }

            return (from IDictionary<string, object> content in (List<object>)files["data"] let type = (string)content["type"] where type == "file" select (string)content["name"]).ToList();
        }
        #endregion

        #region Async

        public async Task<LoadProcess> UploadFileAsync(Stream stream, string cloudFileName)
        {
            return await Task.Run(() => Upload(stream, cloudFileName));
        }

        public async Task<LoadProcess> UploadFileAsync(string localFileName, string cloudFileName)
        {
            return await Task.Run(() => Upload(localFileName, cloudFileName));
        }

        public async Task<LoadProcess> DownloadFileAsync(Stream stream, string cloudFileName)
        {
            return await Task.Run(() => Download(stream, cloudFileName));
        }

        public async Task<LoadProcess> DownloadFileAsync(string localFileName, string cloudFileName)
        {
            return await Task.Run(() => Download(localFileName, cloudFileName));
        }
        #endregion

        #region Auth
        public async Task Authenticate(AccountInfo accountInfo)
        {
            await Authenticate();
        }
        public async Task Authenticate()
        {
            await Task.Run(() =>
            {
                _logger.Info("SkyDrive module: Auth started.");
                var liveAuthClient = new LiveAuthClient("000000004413C061");
                Application.Current.Dispatcher.Invoke(() =>
                {
                    LoginScreen ls = new LoginScreen(liveAuthClient);
                    ls.AuthComplete += SessionGot;
                    ls.Show();
                });
            });
        }
        private void SessionGot(object sender, LiveConnectSession session)
        {
            _logger.Info("SkyDrive module: Session got.");
            _client = new LiveConnectClient(session);
            _logger.Info("SkyDrive module: Auth complete!");
            IsAuthenticated = true;
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
